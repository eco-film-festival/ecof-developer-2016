<?
if(!class_exists('ECOF16_PastEditions_SC_Class'))
{
	class ECOF16_PastEditions_SC_Class {

		public function pastEditions( $atts , $content = null)
		{
			self::register_sc_styles();
    		self::register_sc_scripts();
    		// ------------------------------
    		$sc_atts = shortcode_atts( array(
		        'year' => date("Y"),
		    ), $atts );
    		$year = $sc_atts['year'];
    		// ------------------------------
    		ob_start();
    		?>
    		<div ng-app="appPastEditions">
    			<vd-past-edition year="<?=$year?>"></vd-past-edition>
				<script id="templates/gallery.html" type="text/ng-template">
					<vd-edition-galery data="data.Galeria"></vd-edition-galery>
				</script>
				<script id="templates/sel-oficial.html" type="text/ng-template">
					<vd-list-sel-oficial data="data.SeleccionOficial"></vd-list-sel-oficial>
				</script>
				<script id="templates/jury.html" type="text/ng-template">
					<vd-list-jurys data="data.Jurado"></vd-list-jurys>
				</script>
				<script id="templates/winners.html" type="text/ng-template">
					<vd-list-winners data="data.Ganadores"></vd-list-winners>
				</script>
				<script id="templates/jury-detail.html" type="text/ng-template">
					<vd-jury-detail item="juradoDetail"></vd-jury-detail>
					<div ng-if="false">
						<pre>{{juradoDetail | json}}</pre>
					</div>
				</script>
    		</div>
    		<?
    		$ob_contents = ob_get_contents();
    		ob_end_clean();
    		return $ob_contents;
		}

		public function register_sc_styles() 
		{
    		wp_register_style( 'ecof15_sc_past_editions_css', plugins_url( 'ecof15/css/sc_past_editions.css' ) );
    		wp_enqueue_style( 'ecof15_sc_past_editions_css' );
    		//---------------------------------
    		wp_register_style( 'ecof15_sc_selof_css', plugins_url( 'ecof15/css/sc_selof.css' ) );
    		wp_enqueue_style( 'ecof15_sc_selof_css' );
    		//---------------------------------
    		wp_register_style( 'ecof15_sc_winners_css', plugins_url( 'ecof15/css/sc_winners.css' ) );
			wp_register_style( 'flow-player_css', esc_url_raw( '//releases.flowplayer.org/6.0.3/skin/functional.css' ), array(), null );
			wp_register_style( 'jquery.fancybox.min.css', esc_url_raw( 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.min.css' ), array(), null );
			wp_enqueue_style( 'ecof15_sc_winners_css' );
			wp_enqueue_style( 'flow-player_css' ); 
			wp_enqueue_style( 'jquery.fancybox.min.css' ); 
			//---------------------------------
			wp_register_style( 'ecof15_sc_gallery_css', plugins_url( 'ecof15/css/sc_gallery.css' ) );
    		wp_enqueue_style( 'ecof15_sc_gallery_css' );
			//---------------------------------
		}

  		public function register_sc_scripts() {
    		wp_enqueue_script('ecof16_past_editions', plugins_url('js/sc_past_editions.js', __FILE__));
			wp_enqueue_script('ecof15_sc_winners', plugins_url('js/sc_winners.js', __FILE__));

    		$flowplayer = 'flowplayer.min.js';
			$fancybox = 'jquery.fancybox.js';
		   	$list = 'enqueued';

		    if (wp_script_is( $flowplayer, $list )) {
		     	return;
		    } else {
		       wp_register_script( 'flowplayer.min.js', esc_url_raw( '//releases.flowplayer.org/6.0.3/flowplayer.min.js' ), array(), null );
		       wp_enqueue_script( 'flowplayer.min.js' );
		    }

		    if (wp_script_is( $fancybox, $list )) {
		     	return;
		    } else {
		       wp_register_script( 'jquery.fancybox.js', esc_url_raw( '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.js' ), array(), null );	
		       wp_enqueue_script( 'jquery.fancybox.js' );
		    }
		}		 
	}
}


?>