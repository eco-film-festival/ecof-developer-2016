angular.module('visual-directive', [])
// -------------------------------
    .directive('vdPastEdition', ['$location', function ($location) {
        return {
            restrict: 'E',
            scope: {
                year: '='
            },
            templateUrl: function () {
                var _lang = ($location.absUrl().indexOf('lang=en') > -1);
                return (_lang)
                    ? '/wp-content/plugins/ecof15/js/app/views/vdLastEditionEn.html'
                    : '/wp-content/plugins/ecof15/js/app/views/vdLastEdition.html'
                    ;
            },
            controller: 'ctrlPastEditions',
        };
    }])
    .controller('ctrlPastEditions', ['$scope', '$http', '$q', '$state', '$rootScope', 'anchorSmoothScroll',
        function ($scope, $http, $q, $state, $rootScope, anchorSmoothScroll) {
        $scope.$watch('year', function (v) {
            if (v === undefined) return;
            // ---------------------------------
            $scope.loading = true;
            var _url = 'http://ecofilmfestival.info/api/PublicService/EditionByEdition/' + v;
            $http.get(_url).then(function (response) {
                $scope.loading = false;
                $scope.data = response.data;
                if ($scope.data.Ganadores.length > 0) $state.go('winners');
                else if ($scope.data.SeleccionOficial.length > 0) $state.go('sel-oficial');
                else if ($scope.data.Jurado.length > 0) $state.go('jury');
                else if ($scope.data.Galeria.length > 0) $state.go('gallery');
            });
            // ---------------------------------
            $rootScope.$on('activeJurado', function (e, item) {
                if (item.Informacion_ES !== null && item.Informacion_ES.length > 0) {
                    console.info('Informacion_ES no vacio!');
                    $scope.juradoDetail = item;
                    $state.go('jury-detail');
                    anchorSmoothScroll.scrollTo('marker01');
                } else {
                    console.info('Informacion_ES vacio!');
                }
            });
        });
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('vdListJurys', ['$location', '$rootScope', function ($location, $rootScope) {
        return {
            restrict: 'E',
            scope: {
                data: '='
            },
            templateUrl: function () {
                var _lang = ($location.absUrl().indexOf('lang=en') > -1);
                return (_lang)
                    ? '/wp-content/plugins/ecof15/js/app/views/vdListJurysEn.html'
                    : '/wp-content/plugins/ecof15/js/app/views/vdListJurys.html'
                    ;
            },
            controller: function ($scope) {
                $scope.activeJurado = function (item) {
                    $rootScope.$emit('activeJurado', item);
                };
            }
        };
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('vdJuryDetail', ['$location', '$rootScope', function ($location, $rootScope) {
        return {
            restrict: 'E',
            scope: {
                item: '='
            },
            templateUrl: function () {
                var _lang = ($location.absUrl().indexOf('lang=en') > -1);
                return (_lang)
                    ? '/wp-content/plugins/ecof15/js/app/views/vdJuryDetailEn.html'
                    : '/wp-content/plugins/ecof15/js/app/views/vdJuryDetail.html'
                    ;
            },
            controller: function ($scope) {
                $scope.activeJurado = function (item) {
                    $rootScope.$emit('activeJuradoDetail', item);
                };
            }
        };
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('vdListSelOficial', ['$location', function ($location) {
        return {
            restrict: 'E',
            scope: {
                data: '='
            },
            templateUrl: function () {
                var _lang = ($location.absUrl().indexOf('lang=en') > -1);
                return (_lang)
                    ? '/wp-content/plugins/ecof15/js/app/views/vdListSelOficialEn.html'
                    : '/wp-content/plugins/ecof15/js/app/views/vdListSelOficial.html'
                    ;
            },
            //templateUrl : '/wp-content/plugins/ecof15/js/app/views/vdListSelOficial.html',
            controller: 'ctrlListSelOficial'
        };
    }])
    .controller('ctrlListSelOficial', ['$scope', '$filter', '$location', function ($scope, $filter, $location) {
        $scope.loading = false;
        $scope.filterSelOf = {Categoria: null, Nacionalidad: null};
        $scope.$watch('data', function (v) {
            if (v === undefined) return;

            var _lang = ($location.absUrl().indexOf('lang=en') > -1);
            $scope.listFilterCategorias = $filter('unique')(v, _lang ? 'Categoria_EN' : 'Categoria_ES');
            $scope.listFilterNacionalidad = $filter('unique')(v, _lang ? 'Nacionalidad_EN' : 'Nacionalidad_ES');
            $scope.dataFilter = angular.copy($scope.data);
        });
        $scope.$watch('filterSelOf', function (v) {
            if (v === undefined) return;
            var _lang = ($location.absUrl().indexOf('lang=en') > -1);

            var _listFilter =
                    (_lang) ?
                        $filter('filter')($scope.data, {
                            Categoria_EN: v.Categoria || undefined,
                            Nacionalidad_EN: v.Nacionalidad || undefined
                        }, true)
                        :
                        $filter('filter')($scope.data, {
                            Categoria_ES: v.Categoria || undefined,
                            Nacionalidad_ES: v.Nacionalidad || undefined
                        }, true)
                ;
            $scope.dataFilter = (_listFilter === undefined) ? angular.copy($scope.data) : _listFilter;
        }, true);
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .filter('unique', function () {
        return function (arr, field) {
            var o = {}, i, l = arr.length, r = [];
            for (i = 0; i < l; i += 1) {
                o[arr[i][field]] = arr[i];
            }
            for (i in o) {
                r.push(o[i]);
            }
            return r;
        };
    })
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('vdListWinners', ['$location', function ($location) {
        return {
            restrict: 'E',
            scope: {
                data: '='
            },
            templateUrl: function () {
                var _lang = ($location.absUrl().indexOf('lang=en') > -1);
                return (_lang)
                    ? '/wp-content/plugins/ecof15/js/app/views/vdListWinnersEn.html'
                    : '/wp-content/plugins/ecof15/js/app/views/vdListWinners.html'
                    ;
            },
            controller: 'ctrlListWinners'
        };
    }])
    .controller('ctrlListWinners', ['$scope', '$http', function ($scope, $http) {
        $scope.loading = false;
        $scope.colores = {
            'documental': '#F4524D',
            'animacion': '#24DFDC',
            'ficcion': '#7A24DF',
            'campana': '#DFA224',
        };
    }])
    .directive('winnersItems', [function () {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element, attrs) {
                element.find('.work-item.style-4').hover(function () {
                    angular.element(this).find('img').stop().animate({
                        'top': '-' + angular.element(this).find('.work-info .bottom-meta').outerHeight() / 2 + 'px'
                    }, 250, 'easeOutCubic');
                    angular.element(this).find('.work-info .bottom-meta').addClass('shown').stop().animate({
                        'bottom': '0px'
                    }, 320, 'easeOutCubic');

                }, function () {
                    angular.element(this).find('img').stop().animate({
                        'top': '0px'
                    }, 250, 'easeOutCubic');

                    angular.element(this).find('.work-info .bottom-meta').removeClass('shown').stop().animate({
                        'bottom': '-' + angular.element(this).find('.work-info .bottom-meta').outerHeight() - 2 + 'px'
                    }, 320, 'easeOutCubic');
                });
            }
        };
    }])
    .directive('fancybox', ['$compile', '$timeout', function ($compile, $timeout) {
        return {
            link: function (scope, element, attrs) {
                element.fancybox({
                    tpl: {
                        wrap: '<div class="fancybox-wrap" tabIndex="-1">' +
                        '<div class="fancybox-skin">' +
                        '<div class="fancybox-outer">' +
                        '<div id="player">' + // player container replaces fancybox-inner
                        '</div></div></div></div>'
                    },
                    beforeShow: function () {

                    },
                    beforeClose: function () {

                    }
                });
            }
        };
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('vdEditionDetail', ['$location', function ($location) {
        return {
            restrict: 'E',
            scope: {
                data: '='
            },
            templateUrl: function () {
                var _lang = ($location.absUrl().indexOf('lang=en') > -1);
                return (_lang)
                    ? '/wp-content/plugins/ecof15/js/app/views/vdEditionDetailEn.html'
                    : '/wp-content/plugins/ecof15/js/app/views/vdEditionDetail.html'
                    ;
            },
        };
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('vdEditionSlider', ['$interval', function ($interval) {
        return {
            restrict: 'E',
            scope: {
                data: '=',
                timer: '=',
            },
            templateUrl: '/wp-content/plugins/ecof15/js/app/views/vdEditionSlider.html',
            link: function (scope, elem, attrs) {
                scope.currentIndex = 0;
                scope.next = function () {
                    scope.currentIndex < scope.data.length - 1 ? scope.currentIndex++ : scope.currentIndex = 0;
                };
                scope.prev = function () {
                    scope.currentIndex > 0 ? scope.currentIndex-- : scope.currentIndex = scope.data.length - 1;
                };
                scope.$watch('currentIndex', function (index) {
                    if (scope.data === undefined) return;
                    for (i in scope.data) {
                        scope.data[i].visible = false;
                    }
                    scope.data[index].visible = true;
                });
                var _timer = $interval(function () {
                    scope.next();
                }, scope.timer);
                scope.$on('$destroy', function () {
                    $interval.cancel(_timer);
                })
            }
        };
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('vdEditionGalery', ['$location', function ($location) {
        return {
            restrict: 'E',
            scope: {
                data: '=',
            },
            templateUrl: function () {
                var _lang = ($location.absUrl().indexOf('lang=en') > -1);
                return (_lang)
                    ? '/wp-content/plugins/ecof15/js/app/views/vdEditionGaleryEn.html'
                    : '/wp-content/plugins/ecof15/js/app/views/vdEditionGalery.html'
                    ;
            },
        };
    }])
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .directive('onLastRepeat', function () {
        return function (scope, element, attrs) {

            if (scope.$last) setTimeout(function () {

                (function ($) {
                    /***************** Pretty Photo ******************/

                    function prettyPhotoInit() {

                        //add galleries to portfolios
                        $('.portfolio-items').each(function () {
                            var $unique_id = Math.floor(Math.random() * 10000);
                            $(this).find('.pretty_photo').attr('rel', 'prettyPhoto[' + $unique_id + '_gal]').removeClass('pretty_photo');
                        });

                        $("a[data-rel='prettyPhoto[product-gallery]'], a[data-rel='prettyPhoto']").each(function () {
                            $(this).attr('rel', $(this).attr('data-rel'));
                            $(this).removeAttr('data-rel');
                        });

                        //nectar auto lightbox
                        if ($('body').hasClass('nectar-auto-lightbox')) {
                            $('.gallery').each(function () {
                                if ($(this).find('.gallery-icon a[rel^="prettyPhoto"]').length == 0) {
                                    var $unique_id = Math.floor(Math.random() * 10000);
                                    $(this).find('.gallery-item .gallery-icon a[href*=".jpg"], .gallery-item .gallery-icon a[href*=".png"], .gallery-item .gallery-icon a[href*=".gif"], .gallery-item .gallery-icon a[href*=".jpeg"]').attr('rel', 'prettyPhoto[' + $unique_id + '_gal]').removeClass('pretty_photo');
                                }
                            });
                            $('.main-content img').each(function () {
                                if ($(this).parent().is("[href]") && !$(this).parent().is("[rel*='prettyPhoto']") && $(this).parents('.product-image').length == 0 && $(this).parents('.iosSlider.product-slider').length == 0) {
                                    var match = $(this).parent().attr('href').match(/\.(jpg|png|gif)\b/);
                                    if (match) $(this).parent().attr('rel', 'prettyPhoto');
                                }
                            });
                        }


                        //convert class usage into rel
                        $('a.pp').removeClass('pp').attr('rel', 'prettyPhoto');

                        var loading_animation = ($('body[data-loading-animation]').attr('data-loading-animation') != 'none') ? $('body').attr('data-loading-animation') : null;
                        var ascend_loader = ($('body').hasClass('ascend')) ? '<span class="default-loading-icon spin"></span>' : '';
                        var ascend_loader_class = ($('body').hasClass('ascend')) ? 'default_loader ' : '';
                        $("a[rel^='prettyPhoto']").prettyPhoto({
                            theme: 'dark_rounded',
                            allow_resize: true,
                            default_width: 1024,
                            opacity: 0.85,
                            animation_speed: 'normal',
                            deeplinking: false,
                            default_height: 576,
                            social_tools: '',
                            markup: '<div class="pp_pic_holder"> \
                       <div class="ppt">&nbsp;</div> \
                        <div class="pp_details"> \
                            <div class="pp_nav"> \
                                <a href="#" class="pp_arrow_previous"> <i class="icon-salient-left-arrow-thin icon-default-style"></i> </a> \
                                <a href="#" class="pp_arrow_next"> <i class="icon-salient-right-arrow-thin icon-default-style"></i> </a> \
                                <p class="currentTextHolder">0/0</p> \
                            </div> \
                            <a class="pp_close" href="#"><span class="icon-salient-x icon-default-style"></span></a> \
                        </div> \
                        <div class="pp_content_container"> \
                            <div class="pp_left"> \
                            <div class="pp_right"> \
                                <div class="pp_content"> \
                                    <div class="pp_fade"> \
                                        <div class="pp_hoverContainer"> \
                                        </div> \
                                        <div id="pp_full_res"></div> \
                                        <p class="pp_description"></p> \
                                    </div> \
                                </div> \
                            </div> \
                            </div> \
                        </div> \
                    </div> \
                    <div class="pp_loaderIcon ' + ascend_loader_class + loading_animation + '"> ' + ascend_loader + ' </div> \
                    <div class="pp_overlay"></div>'
                        });

                    }

                    function magnificInit() {

                        //convert old pp links
                        $('a.pp').removeClass('pp').addClass('magnific-popup');
                        $("a[rel^='prettyPhoto']:not([rel*='_gal']):not([rel*='product-gallery']):not([rel*='prettyPhoto['])").removeAttr('rel').addClass('magnific-popup');


                        //add galleries to portfolios
                        $('.portfolio-items').each(function () {
                            if ($(this).find('.pretty_photo').length > 0) {
                                $(this).find('.pretty_photo').removeClass('pretty_photo').addClass('gallery').addClass('magnific');
                            } else if ($(this).find('a[rel*="prettyPhoto["]').length > 0) {
                                $(this).find('a[rel*="prettyPhoto["]').removeAttr('rel').addClass('gallery').addClass('magnific');
                            }

                        });

                        $("a[data-rel='prettyPhoto[product-gallery]']").each(function () {
                            $(this).removeAttr('data-rel').addClass('magnific').addClass('gallery');
                        });

                        //nectar auto lightbox
                        if ($('body').hasClass('nectar-auto-lightbox')) {
                            $('.gallery').each(function () {
                                if ($(this).find('.gallery-icon a[rel^="prettyPhoto"]').length == 0) {
                                    var $unique_id = Math.floor(Math.random() * 10000);
                                    $(this).find('.gallery-item .gallery-icon a[href*=".jpg"], .gallery-item .gallery-icon a[href*=".png"], .gallery-item .gallery-icon a[href*=".gif"], .gallery-item .gallery-icon a[href*=".jpeg"]').addClass('magnific').addClass('gallery').removeClass('pretty_photo');
                                }
                            });
                            $('.main-content img').each(function () {
                                if ($(this).parent().is("[href]") && !$(this).parent().is(".magnific-popup") && $(this).parents('.product-image').length == 0 && $(this).parents('.iosSlider.product-slider').length == 0) {
                                    var match = $(this).parent().attr('href').match(/\.(jpg|png|gif)\b/);
                                    if (match) $(this).parent().addClass('magnific-popup').addClass('image-link');
                                }
                            });
                        }


                        //regular
                        $('a.magnific-popup:not(.gallery):not(.nectar_video_lightbox)').magnificPopup({
                            type: 'image',
                            callbacks: {

                                imageLoadComplete: function () {
                                    var $that = this;
                                    setTimeout(function () {
                                        $that.wrap.addClass('mfp-image-loaded');
                                    }, 10);
                                },
                                beforeOpen: function () {
                                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                                },
                                open: function () {

                                    $.magnificPopup.instance.next = function () {
                                        var $that = this;

                                        this.wrap.removeClass('mfp-image-loaded');
                                        setTimeout(function () {
                                            $.magnificPopup.proto.next.call($that);
                                        }, 100);
                                    };

                                    $.magnificPopup.instance.prev = function () {
                                        var $that = this;

                                        this.wrap.removeClass('mfp-image-loaded');
                                        setTimeout(function () {
                                            $.magnificPopup.proto.prev.call($that);
                                        }, 100);
                                    }

                                }
                            },
                            fixedContentPos: false,
                            mainClass: 'mfp-zoom-in',
                            removalDelay: 400
                        });

                        //video
                        $('a.magnific-popup.nectar_video_lightbox, .swiper-slide a[href*=youtube], .swiper-slide a[href*=vimeo], .nectar-video-box > a.full-link.magnific-popup').magnificPopup({
                            type: 'iframe',
                            fixedContentPos: false,
                            mainClass: 'mfp-zoom-in',
                            removalDelay: 400
                        });


                        //galleries
                        $('a.magnific.gallery').each(function () {

                            var $parentRow = ($(this).parents('.wpb_row').length > 0) ? $(this).parents('.wpb_row') : $(this).parents('.row');
                            if ($parentRow.length > 0 && !$parentRow.hasClass('lightbox-row')) {

                                $parentRow.magnificPopup({
                                    type: 'image',
                                    delegate: 'a.magnific',
                                    mainClass: 'mfp-zoom-in',
                                    fixedContentPos: false,
                                    callbacks: {

                                        elementParse: function (item) {

                                            if ($(item.el.context).is('[href]') && $(item.el.context).attr('href').indexOf('iframe=true') != -1) {
                                                item.type = 'iframe';
                                            } else {
                                                item.type = 'image';
                                            }
                                        },

                                        imageLoadComplete: function () {
                                            var $that = this;
                                            setTimeout(function () {
                                                $that.wrap.addClass('mfp-image-loaded');
                                            }, 10);
                                        },

                                        beforeOpen: function () {
                                            this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                                        },

                                        open: function () {

                                            $.magnificPopup.instance.next = function () {
                                                var $that = this;

                                                this.wrap.removeClass('mfp-image-loaded');
                                                setTimeout(function () {
                                                    $.magnificPopup.proto.next.call($that);
                                                }, 100);
                                            };

                                            $.magnificPopup.instance.prev = function () {
                                                var $that = this;

                                                this.wrap.removeClass('mfp-image-loaded');
                                                setTimeout(function () {
                                                    $.magnificPopup.proto.prev.call($that);
                                                }, 100);
                                            }

                                        }
                                    },
                                    removalDelay: 400,
                                    gallery: {
                                        enabled: true
                                    }
                                });

                                $parentRow.addClass('lightbox-row');
                            }

                        });

                    }

                    function lightBoxInit() {
                        if ($('body[data-ls="pretty_photo"]').length > 0) {
                            prettyPhotoInit();
                        } else if ($('body[data-ls="magnific"]').length > 0) {
                            magnificInit();
                        }
                    }

                    lightBoxInit();
                    //check for late links
                    setTimeout(lightBoxInit, 500);

                })(jQuery);
            }, 1);
        };
    })
    // --------------------------------------------
    // --------------------------------------------
    // --------------------------------------------
    .service('anchorSmoothScroll', function () {

        this.scrollTo = function (eID) {

            // This scrolling function
            // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

            var startY = currentYPosition();
            var stopY = elmYPosition(eID);
            var distance = stopY > startY ? stopY - startY : startY - stopY;
            if (distance < 100) {
                scrollTo(0, stopY);
                return;
            }
            var speed = Math.round(distance / 50);
            if (speed >= 20) speed = 20;
            var step = Math.round(distance / 25);
            var leapY = stopY > startY ? startY + step : startY - step;
            var timer = 0;
            if (stopY > startY) {
                for (var i = startY; i < stopY; i += step) {
                    setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                    leapY += step;
                    if (leapY > stopY) leapY = stopY;
                    timer++;
                }
                return;
            }
            for (var i = startY; i > stopY; i -= step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY -= step;
                if (leapY < stopY) leapY = stopY;
                timer++;
            }

            function currentYPosition() {
                // Firefox, Chrome, Opera, Safari
                if (self.pageYOffset) return self.pageYOffset;
                // Internet Explorer 6 - standards mode
                if (document.documentElement && document.documentElement.scrollTop)
                    return document.documentElement.scrollTop;
                // Internet Explorer 6, 7 and 8
                if (document.body.scrollTop) return document.body.scrollTop;
                return 0;
            }

            function elmYPosition(eID) {
                var elm = document.getElementById(eID);
                var y = elm.offsetTop;
                var node = elm;
                while (node.offsetParent && node.offsetParent != document.body) {
                    node = node.offsetParent;
                    y += node.offsetTop;
                }
                return y;
            }

        };

    })
;