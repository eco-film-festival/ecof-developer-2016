angular.module('app.controllers', [])
.controller('appCtrl', [
  '$scope', '$rootScope', 'AppService', '$filter', '$state',
  function($scope, $rootScope, AppService, $filter, $state) {
      //------------------------
      //------------------------
      $scope.App = {};
      $scope.OfficialSelection = {};
      $scope.loadingOfficialSelection = false;
      $scope.EventDetail = {};
      $scope.Winners = {};
      $scope.loadingWinners = false;
      $scope.Feed = [];
      $scope.filterSchedule = {};
      $scope.ListEventFilter = [];
      $scope.filterOfficialSelection = {Search: ''};
      $scope.activeShortFilm = {};
      $scope.loadingReloadHomeFeed = false;
      //------------------------
      $scope.Host = 'http://ecofilmfestival.info/api/PublicService/';
      //------------------------
      $scope.IsInitialize = false;
      $scope.IsOnline = false;
      $scope.consoleActive = false;
      // ----------------------------
      $scope.$watch('filterSchedule.DateFilter ', function(newNames, oldNames) {
        $scope.filterSchedule.PlaceFilter = null;
        $scope.GetFilterSchedule();
        if ($scope.ListEventFilter !== undefined) {
          $scope.filterEventPlaces = $filter('unique')($scope.ListEventFilter, 'PlaceFilter');
        }
      });

      $scope.$watch('filterSchedule.PlaceFilter ', function(newNames, oldNames) {
        $scope.GetFilterSchedule();
      });

      $scope.GetFilterSchedule = function() {
        var filteredListEvent;
        filteredListEvent = $filter('filter')($scope.App.ListEvent, {
          DateFilter: $scope.filterSchedule.DateFilter || undefined,
          PlaceFilter: $scope.filterSchedule.PlaceFilter || undefined,
        });
        $scope.ListEventFilter = filteredListEvent;
      };

      $scope.$watchCollection('filterOfficialSelection', function() {
        $scope.GetListOfficialSelectionShortFilmFilter();
      });

      $scope.GetListOfficialSelectionShortFilmFilter = function() {
        var filteredListOfficialSelection;
        var filteredListOfficialSelectionBySearch = $filter('filter')($scope.OfficialSelection.ListShortFilm, function(shortFilm) {

          var _titulo = shortFilm.Titulo_Original.toLowerCase();
          var _director = shortFilm.DirectorCorto.toLowerCase();
          var _search = $scope.filterOfficialSelection.Search.toLowerCase();
          return _titulo.indexOf(_search) >= 0 || _director.indexOf(_search) >= 0
        });

        filteredListOfficialSelection = $filter('filter')(filteredListOfficialSelectionBySearch, {
          CategoriaId: $scope.filterOfficialSelection.CategoriaId || undefined,
          NacionalidadId: $scope.filterOfficialSelection.NacionalidadId || undefined,
        });


        $scope.ListOfficialSelectionShortFilmFilter = filteredListOfficialSelection;
      };
      // ----------------------------
      $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            switch(toState.name)
            {
                  case 'home':
                       if(!$scope.IsInitialize)
                       {
                              var _msgSuccess = function(data) {
                                   $scope.IsInitialize = true;
                                   $scope.IsOnline = true;
                                   $scope.GetFilterSchedule();
                                   $scope.loading =  false;
                              };
                              var _msgFail = function(data) {
                                   $scope.IsOnline = false;
                                   $scope.loading =  false;                                  
                              };            
                              var _url = $scope.Host + 'Schedule';
                              $scope.loading =  true;
                              AppService.Service(_url, null, $scope.App).then(_msgSuccess, _msgFail);
                       } 
                  break;
                  case 'event':
                        var _msgSuccess = function(data) {
                              $scope.loading =  false;     
                        };
                        var _msgFail = function(data) {
                              $scope.loading =  false;
                        };  

                        $scope.loading =  true;

                        $filter('filter')($scope.ListEventFilter, function(event) {
                          if (event.Id == toParams.Id && event.TypeFilter == toParams.TypeFilter) {
                              $scope.loading =  true;
                              var _url = $scope.Host + event.DetailUrl;
                              AppService.Service(_url, event.Id, $scope.EventDetail).then(_msgSuccess, _msgFail);
                          }
                        });
   
                  break;
            }

      });

      $scope.viewEvent = function(event) {
            console.info(event);
      };

}])
.controller('appSelOfCtrl', [
  '$scope', '$rootScope', 'AppService', '$filter', '$state',
  function($scope, $rootScope, AppService, $filter, $state) {
      //------------------------
      //------------------------
      $scope.App = {};
      $scope.OfficialSelection = {};
      $scope.loadingOfficialSelection = false;
      $scope.EventDetail = {};
      $scope.Winners = {};
      $scope.loadingWinners = false;
      $scope.Feed = [];
      $scope.filterSchedule = {};
      $scope.ListEventFilter = [];
      $scope.filterOfficialSelection = {Search: ''};
      $scope.activeShortFilm = {};
      $scope.loadingReloadHomeFeed = false;
      //------------------------
      $scope.Host = 'http://ecofilmfestival.info/api/PublicService/';
      //------------------------
      $scope.IsInitialize = false;
      $scope.IsOnline = false;
      $scope.consoleActive = false;
      // ----------------------------
      $scope.$watch('filterSchedule.DateFilter ', function(newNames, oldNames) {
        $scope.filterSchedule.PlaceFilter = null;
        $scope.GetFilterSchedule();
        if ($scope.ListEventFilter !== undefined) {
          $scope.filterEventPlaces = $filter('unique')($scope.ListEventFilter, 'PlaceFilter');
        }
      });

      $scope.$watch('filterSchedule.PlaceFilter ', function(newNames, oldNames) {
        $scope.GetFilterSchedule();
      });

      $scope.GetFilterSchedule = function() {
        var filteredListEvent;
        filteredListEvent = $filter('filter')($scope.App.ListEvent, {
          DateFilter: $scope.filterSchedule.DateFilter || undefined,
          PlaceFilter: $scope.filterSchedule.PlaceFilter || undefined,
        });
        $scope.ListEventFilter = filteredListEvent;
      };

      $scope.$watchCollection('filterOfficialSelection', function() {
        $scope.GetListOfficialSelectionShortFilmFilter();
      });

      $scope.GetListOfficialSelectionShortFilmFilter = function() {
        var filteredListOfficialSelection;
        var filteredListOfficialSelectionBySearch = $filter('filter')($scope.OfficialSelection.ListShortFilm, function(shortFilm) {

          var _titulo = shortFilm.Titulo_Original.toLowerCase();
          var _director = shortFilm.DirectorCorto.toLowerCase();
          var _search = $scope.filterOfficialSelection.Search.toLowerCase();
          return _titulo.indexOf(_search) >= 0 || _director.indexOf(_search) >= 0
        });

        filteredListOfficialSelection = $filter('filter')(filteredListOfficialSelectionBySearch, {
          CategoriaId: $scope.filterOfficialSelection.CategoriaId || undefined,
          NacionalidadId: $scope.filterOfficialSelection.NacionalidadId || undefined,
        });


        $scope.ListOfficialSelectionShortFilmFilter = filteredListOfficialSelection;
      };
      // ----------------------------
      $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            switch(toState.name)
            {
                  case 'home':
                       if(!$scope.IsInitialize)
                       {
                              var _msgSuccess = function(data) {
                                   $scope.loadingOfficialSelection = true;
                                   $scope.IsInitialize = true;
                                   $scope.IsOnline = true;
                                   $scope.GetListOfficialSelectionShortFilmFilter();
                                   $scope.loading =  false;
                              };
                              var _msgFail = function(data) {
                                   $scope.IsOnline = false;
                                   $scope.loading =  false;                                  
                              };            
                              var _url = $scope.Host + 'OfficialSelection';
                              $scope.loading =  true;
                              AppService.Service(_url, null, $scope.OfficialSelection).then(_msgSuccess, _msgFail);
                       } 
                  break;
                  case 'event':
                        var _msgSuccess = function(data) {
                              $scope.loading =  false;     
                        };
                        var _msgFail = function(data) {
                              $scope.loading =  false;
                        };  

                        $scope.loading =  true;

                        $filter('filter')($scope.ListEventFilter, function(event) {
                          if (event.Id == toParams.Id && event.TypeFilter == toParams.TypeFilter) {
                              $scope.loading =  true;
                              var _url = $scope.Host + event.DetailUrl;
                              AppService.Service(_url, event.Id, $scope.EventDetail).then(_msgSuccess, _msgFail);
                          }
                        });
   
                  break;
            }

      });

      $scope.viewEvent = function(event) {
            console.info(event);
      };

}])
.controller('appWinnersCtrl', [
  '$scope', '$rootScope', '$timeout', 'AppService', '$filter', '$state',
  function($scope, $rootScope, $timeout, AppService, $filter, $state) {
      //------------------------
      //------------------------
      $scope.App = {};
      $scope.Winners = {};
      $scope.loadingWinners = false;
      $scope.EventDetail = {};
      $scope.Winners = {};
      $scope.loadingWinners = false;
      $scope.Feed = [];
      $scope.filterSchedule = {};
      $scope.ListEventFilter = [];
      $scope.filterWinners = {Search: ''};
      $scope.activeShortFilm = {};
      $scope.loadingReloadHomeFeed = false;
      //------------------------
      $scope.Host = 'http://ecofilmfestival.info/api/PublicService/';
      //------------------------
      $scope.IsInitialize = false;
      $scope.IsOnline = false;
      $scope.consoleActive = false;
      // ----------------------------
      $scope.colores = ['#F4524D','#24DFDC','#7A24DF','#DFA224','#31B500'];


      $scope.$watch('filterSchedule.DateFilter ', function(newNames, oldNames) {
        $scope.filterSchedule.PlaceFilter = null;
        $scope.GetFilterSchedule();
        if ($scope.ListEventFilter !== undefined) {
          $scope.filterEventPlaces = $filter('unique')($scope.ListEventFilter, 'PlaceFilter');
        }
      });

      $scope.$watch('filterSchedule.PlaceFilter ', function(newNames, oldNames) {
        $scope.GetFilterSchedule();
      });

      $scope.GetFilterSchedule = function() {
        var filteredListEvent;
        filteredListEvent = $filter('filter')($scope.App.ListEvent, {
          DateFilter: $scope.filterSchedule.DateFilter || undefined,
          PlaceFilter: $scope.filterSchedule.PlaceFilter || undefined,
        });
        $scope.ListEventFilter = filteredListEvent;
      };

      $scope.$watchCollection('filterWinners', function() {
        $scope.GetListWinnersShortFilmFilter();
      });

      $scope.GetListWinnersShortFilmFilter = function() {
        var filteredListWinners;
        var filteredListWinnersBySearch = $filter('filter')($scope.Winners.ListShortFilmWinners, function(shortFilm) {

          var _titulo = shortFilm.Titulo_Original.toLowerCase();
          var _director = shortFilm.DirectorCorto.toLowerCase();
          var _search = $scope.filterWinners.Search.toLowerCase();
          return _titulo.indexOf(_search) >= 0 || _director.indexOf(_search) >= 0
        });

        filteredListWinners = $filter('filter')(filteredListWinnersBySearch, {
          CategoriaId: $scope.filterWinners.CategoriaId || undefined,
          NacionalidadId: $scope.filterWinners.NacionalidadId || undefined,
        });


        $scope.ListWinnersShortFilmFilter = filteredListWinners;
      };
      // ----------------------------
      $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            switch(toState.name)
            {
                  case 'home':
                       if(!$scope.IsInitialize)
                       {
                              var _msgSuccess = function(data) {
                                   $scope.loadingWinners = true;
                                   $scope.IsInitialize = true;
                                   $scope.IsOnline = true;
                                   $scope.GetListWinnersShortFilmFilter();
                                   $scope.loading =  false;

                              };
                              var _msgFail = function(data) {
                                   $scope.IsOnline = false;
                                   $scope.loading =  false;                                  
                              };            
                              //var _url = $scope.Host + 'Winners';
                              var _url = $scope.Host + 'Winners';
                              $scope.loading =  true;
                              AppService.Service(_url, null, $scope.Winners).then(_msgSuccess, _msgFail);
                       } 
                  break;
                  case 'event':
                        var _msgSuccess = function(data) {
                              $scope.loading =  false;     
                        };
                        var _msgFail = function(data) {
                              $scope.loading =  false;
                        };  

                        $scope.loading =  true;

                        $filter('filter')($scope.ListEventFilter, function(event) {
                          if (event.Id == toParams.Id && event.TypeFilter == toParams.TypeFilter) {
                              $scope.loading =  true;
                              var _url = $scope.Host + event.DetailUrl;
                              AppService.Service(_url, event.Id, $scope.EventDetail).then(_msgSuccess, _msgFail);
                          }
                        });
   
                  break;
            }

      });
      /*
      $scope.viewWinner = function(winner) {
        
      };
      */


}])
.filter('unique', function() {
  return function(arr, field) {
    var o = {},
    i, l = arr.length,
    r = [];
    for (i = 0; i < l; i += 1) {
      o[arr[i][field]] = arr[i];
    }
    for (i in o) {
      r.push(o[i]);
    }
    return r;
  };
})
.directive('winnersItems', [function() {
    return {
        restrict: 'A',
        scope: false,
        link: function  (scope, element, attrs) {
            element.find('.work-item.style-4').hover(function () {
             angular.element(this).find('img').stop().animate({
                                          'top' : '-'+angular.element(this).find('.work-info .bottom-meta').outerHeight()/2+'px'
                                        },250,'easeOutCubic');
             angular.element(this).find('.work-info .bottom-meta').addClass('shown').stop().animate({
                                          'bottom' : '0px'
                                        },320,'easeOutCubic');

            }, function () {
              angular.element(this).find('img').stop().animate({
                                          'top' : '0px'
                                        },250,'easeOutCubic');

                                        angular.element(this).find('.work-info .bottom-meta').removeClass('shown').stop().animate({
                                          'bottom' : '-'+angular.element(this).find('.work-info .bottom-meta').outerHeight()-2+'px'
                                        },320,'easeOutCubic');
            });

            /*
            var els = angular.element('.portfolio-items .col');
            angular.forEach(els, function( el ){
               if (angular.element(el).has('[data-project-color]')) angular.element(el).find('.work-info-bg, .bottom-meta').css('background-color',angular.element(el).attr('data-project-color'));
            });
            */


        }      
    }
}])
.directive('myRepeatDirective', ['$timeout',function($timeout) {
  return function(scope, element, attrs) {
    
    if (scope.$last){
      console.log("im the last!");
      
      var iso = new Isotope( '.portfolio-items', {
                itemSelector: '.element',
                layoutMode: 'packery'
              });
  
      var cats = angular.element('.portfolio-filters-inline .container ul li a');

      angular.forEach(cats, function( cat ){
               if (angular.element(cat).has('[data-filter]')) angular.element(cat).click(function() {
                iso.arrange({filter:angular.element(cat).attr('data-filter')});
               });
            });

      $timeout(function(){
             iso.resize();
         }, 1000);

      ;

    }
  };
}])
.directive('fancybox',['$compile', '$timeout' , function($compile , $timeout) {
  return {
    link: function(scope , element , attrs){


      console.info('element' , element);

      element.fancybox({
        tpl: {
          wrap: '<div class="fancybox-wrap" tabIndex="-1">' +
                            '<div class="fancybox-skin">' +
                            '<div class="fancybox-outer">' +
                            '<div id="player">' + // player container replaces fancybox-inner
                            '</div></div></div></div>' 
        } , 
        beforeShow : function() {

        },
        beforeClose : function(){

        }
      });
    }
  };  
}])
;
