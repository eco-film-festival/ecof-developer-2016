angular.module( 'app', [ 'app.controllers', 'app.services' ,'ui.router', 'ngAnimate' ] )
	.config(function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('home', {
				url:'/home' ,
				templateUrl: "templates/app-home.html"
			})
			.state('event' , {
				url: "/event/:Id/:TypeFilter",
				templateUrl: "templates/app-event.html"
			})
		;
		$urlRouterProvider.otherwise("/home");
	})	
;
angular.module( 'appSelOf', [ 'app.controllers', 'app.services' ,'ui.router', 'ngAnimate' ] )
	.config(function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('home', {
				url:'/home' ,
				templateUrl: "templates/app-home.html"
			})
			.state('event' , {
				url: "/event/:Id/:TypeFilter",
				templateUrl: "templates/app-event.html"
			})
		;
		$urlRouterProvider.otherwise("/home");
	})	
;
angular.module( 'appWinners', [ 'app.controllers', 'app.services' ,'ui.router', 'ngAnimate' ] )
	.config(function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('home', {
				url:'/home' ,
				templateUrl: "templates/app-home.html"
			})
		;
		$urlRouterProvider.otherwise("/home");
	})	
;

