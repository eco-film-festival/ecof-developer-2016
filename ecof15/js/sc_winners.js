(function($) {    



    var player;

    $('.open-detail-fancybox').fancybox({
        tpl: {
            wrap: '<div class="fancybox-wrap" tabIndex="-1">' +
            '<div class="fancybox-skin">' +
            '<div class="fancybox-outer">' +
            '<div id="player">' + // player container replaces fancybox-inner
            '</div></div></div></div>' 
        },        
        beforeShow: function() {
            //var _id = this.element.attr('id').slice(0,this.element.attr('id').length-4);
            //var _obj = $('#' +  _id);
            var _media = this.element.attr('data-media');//_obj.attr('data-media');
            //this.href = this.element.attr('data-media');
        	this.title = this.element.attr("title");
            player = flowplayer("#player", {
                swf: "//releases.flowplayer.org/6.0.3/flowplayer.swf",
                adaptiveRatio: true,
                autoplay: true,
             	padding : 0,
                clip: {
                    autoplay: true,
                    sources: [
                    	{ type: "video/mp4"  , src: _media }, 
                        { type: "video/flash", src: _media }
					]
                }
            });
        },
        beforeClose: function() {
            player.shutdown();
        }
    });

})(jQuery);