angular.module('appPastEditions' , ['ui.router', 'ngAnimate' , 'visual-directive'])
	.config(function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('sel-oficial' , {
				url : '/sel-oficial',
				templateUrl:'templates/sel-oficial.html',
			})
			.state('jury' , {
				url : '/jury',
				templateUrl:'templates/jury.html',
			})
			.state('jury-detail' , {
				url : '/jury-detail',
				templateUrl:'templates/jury-detail.html',
			})
			.state('winners' , {
				url : '/winners',
				templateUrl:'templates/winners.html',
			})
			.state('gallery' , {
				url : '/gallery',
				templateUrl:'templates/gallery.html',
			})
		;
		$urlRouterProvider.otherwise("/winners");
	})
;