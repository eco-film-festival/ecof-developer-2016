jQuery(document).ready(function() {
	jQuery("#owl-list-sponsor").owlCarousel({
		autoPlay: true,
		items : 5,
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [979,3]
	});
});

// ---------------------------------
angular.module('appSponsor' , ['ui.router', 'ngAnimate' , 'visual-directive'])
	.controller('ctrlSponsorEditions' , ['$scope' , '$http' , '$q' , function($scope , $http , $q){
		// ---------------------------------
		$scope.$watch('year' , function(v){
			if(v === undefined) return;
			// ---------------------------------
			$scope.loading = true;
			var _url = 'http://ecofilmfestival.info/api/PublicService/SponsorByEdition/' + v;
			$http.get(_url).then(function(response){
				$scope.loading = false;
				$scope.data = response.data;
			});
			// ---------------------------------


		});

	}])

;