<?
if(!class_exists('ECOF16_PastEditions_SC_Class'))
{
	class ECOF16_PastEditions_SC_Class {

		public function ecof16_past_editions( $atts , $content = null)
		{
			self::register_sc_styles();
    		self::register_sc_scripts();

    		ob_start();
    		?>
    		<?
    		$ob_contents = ob_get_contents();
    		ob_end_clean();
    		return $ob_contents;
		}

		public function register_sc_styles() 
		{
    		wp_register_style( 'ecof15_sc_past_editions_css', plugins_url( 'ecof15/css/sc_past_editions.css' ) );
    		wp_enqueue_style( 'ecof15_sc_past_editions_css' );
  		}

  		public function register_sc_scripts() {
    		wp_enqueue_script('ecof16_past_editions', plugins_url('js/sc_past_editions.js', __FILE__));
  		}		 
	}
}

?>