<?
if(!class_exists('ECOF16_Sponsor_SC_Class'))
{

	class ECOF16_Sponsor_SC_Class {
		
		public function listSponsorHome($atts , $content = null)
		{
			$sc_atts = shortcode_atts( array(
		        'service' => 'http://ecofilmfestival.info/api/PublicService/SponsorHome',
		    ), $atts );
			//---------------------------
			self::register_sc_styles();
			self::register_sc_scripts();
			//---------------------------
			$json = file_get_contents($sc_atts['service']);
			$result = json_decode($json , true );
			//---------------------------
			ob_start();
			?>
			<div class="container">
				<div class="row">
					<div class="span12">
						<div id="owl-list-sponsor" class="owl-carousel">
							<?
							foreach ($result as $typeSponsor) {
								foreach ($typeSponsor['ListSponsor'] as $sponsor) {
									$item = '
									<div class="item">
										<a href="'.$sponsor['WebSite'].'" target="_blank">
											<img src="'.$sponsor['Image'].'" alt="'.$sponsor['Title_ES'].'"/>
										</a>
									</div>
									';
									echo $item;
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
			<?
			$ob_contents = ob_get_contents();
			ob_end_clean();
			return $ob_contents;
		}

		public function listSponsorEdition($atts , $content = null)
		{
			// ------------------------------
    		$sc_atts = shortcode_atts( array(
		        'year' => date("Y"),
		    ), $atts );
    		$year = $sc_atts['year'];
    		$lang = (!empty($_GET['lang'])) ? 'EN': 'ES';
    		// ------------------------------
			self::register_sc_styles();
			self::register_sc_scripts();
			//---------------------------
			ob_start();
			?>
			<div class="container">
				<div ng-app="appSponsor" ng-controller="ctrlSponsorEditions" ng-init="year = <?=$year?>">
					<div class="row wpb_row vc_row-fluid standar_section" ng-repeat="list in data track by $index">
						<h1 class="title-sponsor">{{list.Descripcion_<?=$lang?>}}</h1>
						<div ng-switch="list.ListSponsor.length">
							<div ng-switch-when="2">
								<div ng-repeat="item in list.ListSponsor track by $index" ng-switch="$index%2" class="listSponsor">
									<div class="col span_12" ng-switch-when="0">
										<div class="vc_col-sm-6 wpb_column column_container col one-fourths clear-both" >
											<div class="wpb_wrapper">
												<div style="text-align: center;">
													<a ng-href="{{list.ListSponsor[$index + 0].WebSite}}" target="_blank">
													<img ng-src="{{list.ListSponsor[$index + 0].Image}}"/>
													</a>
												</div>
											</div> 
										</div> 
										<div class="vc_col-sm-6 wpb_column column_container col one-fourths clear-both" >
											<div class="wpb_wrapper">
												<div>
													<a ng-href="{{list.ListSponsor[$index + 1].WebSite}}" target="_blank">
														<img ng-src="{{list.ListSponsor[$index + 1].Image}}"/>
													</a>
												</div>
											</div> 
										</div> 
									</div>
								</div>
							</div>
							<div ng-switch-default>
								<div ng-repeat="item in list.ListSponsor track by $index" ng-switch="$index%3" class="listSponsor">
									<div class="col span_12" ng-switch-when="0">
										<div class="vc_col-sm-4 wpb_column column_container col one-fourths clear-both" >
											<div class="wpb_wrapper">
												<div class="display-gallery-item">
													<a ng-href="{{list.ListSponsor[$index + 0].WebSite}}" target="_blank">
													<img ng-src="{{list.ListSponsor[$index + 0].Image}}"/>
													</a>
												</div>
											</div> 
										</div> 
										<div class="vc_col-sm-4 wpb_column column_container col one-fourths clear-both" >
											<div class="wpb_wrapper">
												<div class="display-gallery-item">
													<a ng-href="{{list.ListSponsor[$index + 1].WebSite}}" target="_blank">
														<img ng-src="{{list.ListSponsor[$index + 1].Image}}"/>
													</a>
												</div>
											</div> 
										</div> 
										<div class="vc_col-sm-4 wpb_column column_container col one-fourths clear-both" >
											<div class="wpb_wrapper">
												<div class="display-gallery-item">
													<a ng-href="{{list.ListSponsor[$index + 2].WebSite}}" target="_blank">
														<img ng-src="{{list.ListSponsor[$index + 2].Image}}"/>
													</a>
												</div>
											</div> 
										</div> 
									</div>
								</div>
							</div>
						</div>

							
					</div>
				</div>
			</div>
			<?
			$ob_contents = ob_get_contents();
			ob_end_clean();
			return $ob_contents;
		}


		public function register_sc_styles() 
		{
			wp_register_style( 'eco_sc_sponsor_css' , plugins_url( '/ecof15/css/sc_sponsor.css' ) );
			wp_enqueue_style( 'eco_sc_sponsor_css' );
			wp_register_style( 'tp-carousel-css' , plugins_url( '/ecof15/js/plugins/owl.carousel/owl.carousel.css' ) );
			wp_enqueue_style( 'tp-carousel-css' );
			wp_register_style( 'tp-carousel-theme-css' , plugins_url( '/ecof15/js/plugins/owl.carousel/owl.theme.css' ) );
			wp_enqueue_style( 'tp-carousel-theme-css' );
		}

		public function register_sc_scripts()
		{
			wp_enqueue_script('jquery');
			wp_enqueue_script('tp-carousel-js', plugins_url( '/js/plugins/owl.carousel/owl.carousel.js', __FILE__ ), array('jquery'), '1.0', false);
			wp_enqueue_script('ecof15_sc_sponsor', plugins_url('/js/sc_sponsor.js', __FILE__));
		}

	}


}

?>