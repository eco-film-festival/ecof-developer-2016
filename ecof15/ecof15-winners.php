<?
class ECOF15_Winners_SC_Class{  

	public function ecof15_winners( $atts, $content = null )
	{

		self::register_sc_styles();
		self::register_sc_scripts();

		ob_start();
		?>
		<div id="winners" ng-app="appWinners">      
			<div ng-controller="appWinnersCtrl"> 
				<!-- loader -->
				<center ng-show="loading" class="fade">
					<h5>
						<i class="icon-refresh icon-spin icon-medium" style=" color: white!important; "></i><br />
						<span style=" font-weight: 300; font-family: 'Open Sans'; ">loading ...</span>
					</h5>
				</center>        
				<!--/loader -->
				<div ui-view class="fade"></div>		
			</div>
		<script id="templates/app-home.html" type="text/ng-template">
					<div class="row fade" ng-show="!loading">
						<div class="col span_12 dark left">
							<div class="vc_col-sm-12 wpb_column column_container col no-extra-padding instance-0" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">

								<div class="wpb_wrapper">
								<!-- filters -->
								<div class="portfolio-filters-inline full-width-section non-fw first-section bg-only" data-color-scheme="default" data-image-height="61" style="margin-left: 0px; margin-right: 0px; padding: 0px; visibility: visible; margin-top: 0px!important; height: 61px; background-color: #2E133F !important;" instance="0">
									<div class="container" style=" padding: 0 3em; ">
										<span id="current-category">All</span>
										<ul>
											<li id="sort-label">Filtrar por:</li>
											<li><a  data-filter="*" class="active">Todos</a></li>
											<li><a  data-filter=".cat_1">Documental</a></li>
											<li><a  data-filter=".cat_2">Animación</a></li>
											<li><a  data-filter=".cat_3">Ficción</a></li>
											<li><a  data-filter=".cat_4">Campaña Audiovisual</a></li>
										</ul>
										<div class="clear"></div>
									</div>
								</div>
								<!--/filters -->
								<div style="height: 40px;" class="divider"></div>

								<div class="portfolio-wrap ">
									<span class="portfolio-loading  " style="display: none;">   </span>
									<div class="row portfolio-items " data-col-num="cols-3" instance="0" style="">
										<!-- portfolio-content -->									

										<div 
											class="col span_4  element cat_{{obj.Categoria.Id}} " 
											data-project-cat="cat_{{obj.Categoria.Id}} " 
											data-project-color="{{colores[obj.Categoria.Id-1]}}" 
											data-title-color="" 
											data-subtitle-color="" 
											data-ng-repeat="obj in ListWinnersShortFilmFilter track by $index" 
											winners-items 
											my-repeat-directive
											>						

											<div class="work-item style-4" data-custom-content="" style="opacity: 1;">												
												<img width="600" height="403" ng-src="{{obj.Still.Host + obj.Still.Path + obj.Still.File}}" class="attachment-portfolio-thumb wp-post-image" alt="spacebound-wide" title="" style="top: 0px;">

												<div style="" class="winner_resume">
														<div class="winner_des"><i class="fa fa-trophy"></i>&nbsp;{{ obj.Descripcion_Ganador_ES }}</div>
														<div class="winner_cat" style="background-color:{{colores[obj.Categoria.Id-1]}};">{{ obj.Categoria.Title_ES }}</div>
														<div class="winner_dir">{{ obj.DirectorCorto }}</div>
														<div class="winner_nac">{{ obj.Nacionalidad.Title_ES }}</div>
													</div>

												<div class="work-info" style="opacity: 0;">																						

													<div class="bottom-meta" style="background-color:{{colores[obj.Categoria.Id-1]}};">
														
														<div class="row ecof-no-padding ecof-no-margin ecof-no-bg">
															<div class="col span_12 ecof-no-padding ecof-no-margin ecof-no-bg">
																	
																	<!-- open-detail-fancybox -->																	
																	<h3>{{obj.Titulo_Original}}</h3>
																	<p>{{obj.Descripcion_Ganador_ES}}</p>
																	<a class="open-detail-fancybox button"
																	id="{{obj.Video}}"
																	title="{{obj.Titulo_Original}}"
																	ng-href="#{{obj.Video}}"
																	data-media="{{ obj.Host + '' + obj.Path + '' + obj.Video }}" >
																		<i class="fa fa-film fa-2x icon-film"></i>
																	</a>
																	<!--/open-detail-fancybox -->

															</div>
														</div>
													</div><!--/bottom-meta-->		

													<div style=" width: 0; height: 0; border-left: 20px solid transparent; border-right: 20px solid transparent; border-top: 20px solid {{colores[obj.Categoria.Id-1]}}; margin: 0 auto; "></div>
													

												</div>

											</div><!--work-item-->
										</div>
										
										<!--/portfolio-content -->
									</div><!--/portfolio-->
								</div><!--/portfolio wrap-->

							</div><!--/wpb_wrapper-->
						</div><!--/vc_col--> 						
					</div><!--/col-->
		</script>		
		<script id="templates/winners-item.html" type="text/ng-template">	
			<div class="col span_4 element video" data-project-cat="video" data-project-color="#8a6f7e">						
				<div class="work-item style-4">	
					<img width="600" height="403" src="http://themenectar.com/demo/salient-frostwave/wp-content/uploads/2014/03/ownage21-600x403.jpg" class="attachment-portfolio-thumb wp-post-image" alt="as " title="as " >
					<div class="work-info">

						<a href="#"></a>

						<div class="bottom-meta">
							<h3>Demo</h3> 
							asas ss
						</div><!--/bottom-meta-->

					</div>
				</div><!--work-item-->
			</div>					
		</script>
		</div>
	<?
	$ob_contents = ob_get_contents();
	ob_end_clean();
		//end : object buffer
	return $ob_contents;
}

public function register_sc_styles() {

	wp_register_style( 'ecof15_sc_winners_css', plugins_url( 'ecof15/css/sc_winners.css' ) );
	wp_register_style( 'flow-player_css', esc_url_raw( '//releases.flowplayer.org/6.0.3/skin/functional.css' ), array(), null );
	wp_register_style( 'jquery.fancybox.min.css', esc_url_raw( 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.min.css' ), array(), null );
	//https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.min.css
	wp_enqueue_style( 'ecof15_sc_winners_css' );
	wp_enqueue_style( 'flow-player_css' ); 
	wp_enqueue_style( 'jquery.fancybox.min.css' ); 
}

public function register_sc_scripts() {

	$flowplayer = 'flowplayer.min.js';
	$fancybox = 'jquery.fancybox.js';
   	$list = 'enqueued';

    if (wp_script_is( $flowplayer, $list )) {
     	return;
    } else {
       wp_register_script( 'flowplayer.min.js', esc_url_raw( '//releases.flowplayer.org/6.0.3/flowplayer.min.js' ), array(), null );
       wp_enqueue_script( 'flowplayer.min.js' );
    }

    if (wp_script_is( $fancybox, $list )) {
     	return;
    } else {
       wp_register_script( 'jquery.fancybox.js', esc_url_raw( '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.js' ), array(), null );	
       wp_enqueue_script( 'jquery.fancybox.js' );
    }
	
	wp_enqueue_script('ecof15_sc_winners', plugins_url('js/sc_winners.js', __FILE__));

} 

}
?>