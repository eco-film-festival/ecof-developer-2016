<?
class ECOF15_SelOf_SC_Class{  

  public function ecof15_selof( $atts, $content = null )
  {

    self::register_sc_styles();
    self::register_sc_scripts();

    ob_start();
    ?>
    <div id="selof" ng-app="appSelOf">      
      <div ng-controller="appSelOfCtrl"> 
        <!-- loader -->
        <center ng-show="loading" class="fade">
          <h5>
            <i class="icon-refresh icon-spin icon-medium" style=" color: white!important; "></i><br />
            <span style=" font-weight: 300; font-family: 'Open Sans'; ">loading ...</span>
          </h5>
        </center>        
        <!--/loader -->
        <div ui-view class="fade"></div>      

        <script id="templates/app-home.html" type="text/ng-template">
        <!-- resultados -->
        <!-- filtros -->
        <div class="row fade" ng-show="!loading">
          <div class="col span_4">
            <div class="input-label ecof15_label">
              FILTRAR POR CATEGORÍA
            </div>
            <select ng-model="filterOfficialSelection.CategoriaId" ng-options="option.Id as option.Title_ES for option in OfficialSelection.ListCategoria">
              <option value>-- Todos --</option>
            </select>
          </div>
          <div class="col span_4">
            <div class="input-label ecof15_label">
              FILTRAR POR NACIONALIDAD
            </div>
            <select ng-model="filterOfficialSelection.NacionalidadId" ng-options="option.Id as option.Title_ES for option in OfficialSelection.ListNacionalidad">
              <option value>-- Todos --</option>
            </select>
          </div>
          <div class="col span_4 col_last">            
            <!--<input type="text" placeholder="Buscar" ng-model="filterOfficialSelection.Search">-->
          </div>        
        </div>
        <!--/filtros -->
        <!-- items-programa -->
        <div class="row fade" ng-show="!loading">          
          <div class="col span_12">

            <table class="table ecof15_table">
              <thead>
                <tr>
                  <th>Categoría</th>
                  <th>Titulo</th>
                  <th>Dirección</th>
                  <th>Nacionalidad</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="event in ListOfficialSelectionShortFilmFilter">
                  <td>{{event.Categoria.Title_ES}}</td>
                  <td style="font-weight:900;">
                    {{ event.Titulo_Original }}<br/>                    
                  </td>
                  <td>{{ event.DirectorCorto }}</td>
                  <td>{{event.Nacionalidad.Title_ES}}</td>                                    
                </tr>
              </tbody>
            </table>

          </div>
        </div>
        <!--/items-programa -->        
        <code data-ng-if="consoleActive">
          filterOfficialSelection = {{ filterOfficialSelection | json }}
        </code>        
        <code data-ng-if="consoleActive">
          ListOfficialSelectionShortFilmFilter = {{ ListOfficialSelectionShortFilmFilter | json }}
        </code>
        </script>
        <!--/resultados -->
        <script id="templates/app-event.html" type="text/ng-template">
        <!-- evento (EventDetail) -->    
        <div class="row fade" ng-show="!loading" >
          <div class="col span_12">
            <!-- header -->
            <div class="row padding-bottom-0" style="background-color: white;">
              <div class="col span_10 ecof_col padding-10" style="">
                <i class="icon-calendar icon-medium pull-left"></i><span class="ecof_subtitle1"> {{EventDetail.Date.Day}}/{{EventDetail.Date.MonthString}}</span>                
              </div>      
              <div class="col span_2 ecof_col padding-10">
                <a class="back-btn fade" ui-sref="home" ng-if="!loading" ><i class="icon-arrow-left icon-medium pull-right"></i></a>
              </div>
            </div>
            <!--/header -->
            <!-- titulo -->
            <div class="row padding-bottom-0 ecof_bg_color1">
              <div class="col span_8 ecof_col padding-10" >
                <span class="ecof_title1">{{EventDetail.Event_ES}}</span>
              </div>
              <div class="col span_4 col_last ecof_col padding-10">
                <span class="ecof_title1">{{EventDetail.Type_ES}}</span>  
              </div>          
            </div>
            <!--/titulo -->
            <!-- detalles -->
            <div class="row ecof_detail1"
                 ng-if="EventDetail.ImageWeb.length > 0"
                 style="background-color: #F8F8F8; background-image: url({{EventDetail.ImageWeb}}); background-size: cover; background-position: center center; min-height: 400px;" >              
            </div>
            <div class="row ecof_detail1" style="background-color: #FFFFFF;" ng-if="EventDetail.Description_ES.length > 0">
              <div class="col span_12 ecof_col padding-10" style="">
                <span class="ecof_subtitle_label">INFO:</span><br/> 
                <span class="ecof_subtitle2">{{EventDetail.Description_ES}}</span>
              </div>
            </div>
            <div class="row ecof_detail1" style="background-color: #F4F4F4;" >
              <div class="col span_4 ecof_col padding-10" style="">
                <span class="ecof_subtitle_label">SEDE:</span><br/> 
                <span class="ecof_subtitle2">{{EventDetail.Place.Title_ES}}</span>
              </div>
              <div class="col span_4 ecof_col padding-10">
                <a ng-href="{{EventDetail.Place.Url}}" target="_blank">
                  <img class="ecof_icon_map pull-right" src="http://ecofilmfestival.org/wp-content/uploads/2015/10/google-maps-icon.png" />
                </a>
                <span class="ecof_subtitle_label">LUGAR:</span><br/> 
                <span class="ecof_subtitle2">{{EventDetail.Site_ES}}</span>                
              </div>
              <div class="col span_4 col_last ecof_col padding-10">
                <span class="ecof_subtitle_label">HORARIO:</span><br/> 
                <span class="ecof_subtitle2">{{EventDetail.Start}} a {{EventDetail.End}}</span>
              </div>          
            </div>
            <!--/detalles -->   
            <div class="row ecof_detail1"  style="height: 40px;" >              
              <div class="col span_12 col_last ecof_col padding-10">
                &nbsp;                
              </div>          
            </div>         
            <!-- seleccion oficial -->            
            <div class="row ecof_detail1" style="background-color: white;" ng-animate="{enter: 'animate-enter', leave: 'animate-leave'}"
                ng-if="$even" 
                data-ng-repeat="(key, value) in EventDetail.ListShortFilms" >              
              
              <div class="col span_6 ecof_col padding-10 fade" ng-if="EventDetail.ListShortFilms[$index].Titulo_Espanol.length > 0">
                <span class="ecof_subtitle3">{{EventDetail.ListShortFilms[$index].Titulo_Espanol}}</span>
                <div style="background-image: url({{EventDetail.ListShortFilms[$index].ListImagenStill[0].Host + EventDetail.ListShortFilms[$index].ListImagenStill[0].Path + EventDetail.ListShortFilms[$index].ListImagenStill[0].File}}); background-size: cover; background-position: center center;" class="ecof_short_still" ></div>                
                <span class="ecof_subtitle5"><span>Dirección</span> {{EventDetail.ListShortFilms[$index].DirectorCorto}}</span>                
                <span class="ecof_subtitle4">
                  {{EventDetail.ListShortFilms[$index].Categoria.Title_ES}} / {{EventDetail.ListShortFilms[$index].Nacionalidad.Title_ES}}
                </span>
              </div>
              
              <div class="col span_6 ecof_col padding-10 col_last fade" ng-if="EventDetail.ListShortFilms[$index+1].Titulo_Espanol.length > 0">
                <span class="ecof_subtitle3">{{EventDetail.ListShortFilms[$index+1].Titulo_Espanol}}</span>
                <div style="background-image: url({{EventDetail.ListShortFilms[$index+1].ListImagenStill[0].Host + EventDetail.ListShortFilms[$index+1].ListImagenStill[0].Path + EventDetail.ListShortFilms[$index+1].ListImagenStill[0].File}}); background-size: cover; background-position: center center;" class="ecof_short_still" ></div>
                <span class="ecof_subtitle5"><span>Dirección</span> {{EventDetail.ListShortFilms[$index+1].DirectorCorto}}</span>
                <span class="ecof_subtitle4">
                  {{EventDetail.ListShortFilms[$index+1].Categoria.Title_ES}} / {{EventDetail.ListShortFilms[$index+1].Nacionalidad.Title_ES}}
                </span>
              </div>

            </div>
            <!--/seleccion oficial -->
            <!-- muestra -->            
            <div class="row ecof_detail1" style="background-color: white;" ng-animate="{enter: 'animate-enter', leave: 'animate-leave'}"
                ng-if="$even" 
                data-ng-repeat="(key, value) in EventDetail.ListShortFilmsMuestra" >              
              <div class="col span_6 ecof_col padding-10 fade" ng-if="EventDetail.ListShortFilmsMuestra[$index].Titulo_Espanol.length > 0">
                <span class="ecof_subtitle6">{{EventDetail.ListShortFilmsMuestra[$index].Titulo_Espanol}}</span>
                <div style="background-image: url({{EventDetail.ListShortFilmsMuestra[$index].ListImagenStill[0].Host + EventDetail.ListShortFilmsMuestra[$index].ListImagenStill[0].Path + EventDetail.ListShortFilmsMuestra[$index].ListImagenStill[0].File}}); background-size: cover; background-position: center center;" class="ecof_short_still" ></div>                
                <span class="ecof_subtitle7"><span>Dirección</span> {{EventDetail.ListShortFilmsMuestra[$index].Director}}</span>                
                <span class="ecof_subtitle8">
                  Muestra / {{EventDetail.ListShortFilmsMuestra[$index].Pais}}
                </span>
              </div>
              
              <div class="col span_6 ecof_col padding-10 col_last fade" ng-if="EventDetail.ListShortFilmsMuestra[$index+1].Titulo_Espanol.length > 0">
                <span class="ecof_subtitle6">{{EventDetail.ListShortFilmsMuestra[$index+1].Titulo_Espanol}}</span>
                <div style="background-image: url({{EventDetail.ListShortFilmsMuestra[$index+1].ListImagenStill[0].Host + EventDetail.ListShortFilmsMuestra[$index+1].ListImagenStill[0].Path + EventDetail.ListShortFilmsMuestra[$index+1].ListImagenStill[0].File}}); background-size: cover; background-position: center center;" class="ecof_short_still" ></div>
                <span class="ecof_subtitle7"><span>Dirección</span> {{EventDetail.ListShortFilmsMuestra[$index+1].Director}}</span>
                <span class="ecof_subtitle8">
                  Muestra / {{EventDetail.ListShortFilmsMuestra[$index+1].Pais}}
                </span>
              </div>

            </div>
            <!--/muestra -->            
          </div>
        </div>
        <!--/evento -->
        </script>
        
      </div>
    </div>
    <?
    $ob_contents = ob_get_contents();
    ob_end_clean();
    //end : object buffer
    return $ob_contents;
  }

  public function register_sc_styles() {

    wp_register_style( 'ecof15_sc_selof_css', plugins_url( 'ecof15/css/sc_selof.css' ) );
    wp_enqueue_style( 'ecof15_sc_selof_css' );
  }

  public function register_sc_scripts() {
    wp_enqueue_script('ecof15_sc_selof', plugins_url('js/sc_selof.js', __FILE__));
  } 

}
?>
