<?php
/*
Plugin Name: ECOFILM 2015
Plugin URI: http://ecofilmfestival.org/
Description: Plugin Ecofilm 2015
Version: 1.0
Author: zowarin@gmail.com
Author URI: http://krkn.solutions
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
require 'ecof15-programa.php';
require 'ecof15-selof.php';
require 'ecof15-winners.php';
//-------------------------------
require 'ecof15-sponsor.php';
//-------------------------------
require 'ecof16-past-editions.php';
//-------------------------------

add_action(
	'plugins_loaded',
	array ( ECOF15_Plugin_Class::get_instance(), 'plugin_setup' )
);


class ECOF15_Plugin_Class
{
	

	private $programa = null;
	private $selof = null;
	private $winners = null;
	private $sponsor = null;
	private $editions = null;
	/**
	 * Plugin instance.
	 *
	 * @see get_instance()
	 * @type object
	 */
	protected static $instance = NULL;
	/**
	 * URL to this plugin's directory.
	 *
	 * @type string
	 */
	public $plugin_url = '';
	/**
	 * Path to this plugin's directory.
	 *
	 * @type string
	 */
	public $plugin_path = '';
	/**
	 * Access this plugin’s working instance
	 *
	 * @wp-hook plugins_loaded
	 * @since   2012.09.13
	 * @return  object of this class
	 */
	public static function get_instance()
	{
		NULL === self::$instance and self::$instance = new self;
		return self::$instance;
	}
	/**
	 * Used for regular plugin work.
	 *
	 * @wp-hook plugins_loaded
	 * @since   2012.09.10
	 * @return  void
	 */
	public function plugin_setup()
	{
		$this->plugin_url    = plugins_url( '/', __FILE__ );
		$this->plugin_path   = plugin_dir_path( __FILE__ );


		add_action( 'wp_enqueue_scripts', array( &$this, 'register_plugin_styles' ) );
		add_action( 'wp_enqueue_scripts', array( &$this, 'register_plugin_scripts' ) );

		$this->programa = new ECOF15_Programa_SC_Class();
		add_shortcode('ecof15_programa', array( &$this->programa, 'ecof15_programa') );

		$this->selof = new ECOF15_SelOf_SC_Class();
		add_shortcode('ecof15_selof', array( &$this->selof, 'ecof15_selof') );

		$this->winners = new ECOF15_Winners_SC_Class();
		add_shortcode('ecof15_winners', array( &$this->winners, 'ecof15_winners') );
		


		// ------------------------------------------------
		// ------------------------------------------------
		// ------------------------------------------------
		$this->sponsor = new ECOF16_Sponsor_SC_Class();
		add_shortcode('ecof_sponsor_home' , array(&$this->sponsor , 'listSponsorHome'));
		add_shortcode('ecof_sponsor_edition' , array(&$this->sponsor , 'listSponsorEdition'));
		// ------------------------------------------------
		// ------------------------------------------------
		// ------------------------------------------------
		$this->editions = new ECOF16_PastEditions_SC_Class();
		add_shortcode('ecof_past_editions' , array(&$this->editions , 'pastEditions'));
		// ------------------------------------------------
		// ------------------------------------------------
		// ------------------------------------------------
	}
	/**
	 * Constructor. Intentionally left empty and public.
	 *
	 * @see plugin_setup()
	 * @since 2012.09.12
	 */
	public function __construct() {}
	/**
	 * Loads translation file.
	 *
	 * Accessible to other classes to load different language files (admin and
	 * front-end for example).
	 *
	 * @wp-hook init
	 * @param   string $domain
	 * @since   2012.09.11
	 * @return  void
	 */
	public function load_language( $domain )
	{
		load_plugin_textdomain(
			$domain,
			FALSE,
			$this->plugin_path . 'languages'
		);
	}
	/**
	* Loads style files.
	*
	* @see plugin_setup()
	* @since 2015.09.28
	*/
	public function register_plugin_styles() {
	}
	/**
	* Loads script files.
	*
	* @see plugin_setup()
	* @since 2015.09.28
	*/
	public function register_plugin_scripts() {
		wp_register_script( 'ecof15_angular', plugins_url( 'ecof15/js/angular/angular.min.js' ) );
		wp_register_script( 'ecof15_angular-animate', plugins_url( 'ecof15/js/angular/angular-animate.min.js' ) );
		wp_register_script( 'ecof15_angular-ui-router', plugins_url( 'ecof15/js/angular/angular-ui-router.min.js' ) );
		// ------------------------------------------------
		// ------------------------------------------------
		// ------------------------------------------------
		wp_register_script( 'ecof16_visual-directive', plugins_url( 'ecof15/js/app/visual-directive.js' ) );		
		// ------------------------------------------------
		// ------------------------------------------------
		// ------------------------------------------------
		wp_register_script( 'ecof15_app', plugins_url( 'ecof15/js/app/app.js' ) );
		wp_register_script( 'ecof15_controllers', plugins_url( 'ecof15/js/app/controllers.js' ) );
		wp_register_script( 'ecof15_services', plugins_url( 'ecof15/js/app/services.js' ) );
		// ------------------------------------------------
		// ------------------------------------------------
		// ------------------------------------------------		
		wp_register_script( 'fancybox', ( '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.js' ), false, null, true );				
		wp_register_script( 'flowplayer', ( '//releases.flowplayer.org/6.0.3/flowplayer.min.js' ), false, null, true );				//		
		// -----------------------------------------		
		// -----------------------------------------		
		// -----------------------------------------
		wp_enqueue_script('ecof15_angular');
		wp_enqueue_script('ecof15_angular-animate');
		wp_enqueue_script('ecof15_angular-ui-router');
		wp_enqueue_script('ecof16_visual-directive');

		wp_enqueue_script('ecof15_app');
		wp_enqueue_script('ecof15_controllers');
		wp_enqueue_script('ecof15_services');

		wp_enqueue_script( 'fancybox' );				
		wp_enqueue_script( 'flowplayer' );
	}
}
