<?

if(!class_exists('WP_Plugin_Ecofim_Service_Programa'))
{
	class WP_Plugin_Ecofim_Service_Programa extends WP_Plugin_Ecofim_Service_csv
	{		
		
		private function filter ($data , $default_date , $last_date , $date , $all = 'true') {
			
			if($all == 'true') return $data;
			
			$f_date = new DateTime(str_replace('/','-',$date));
			$f_last_date = new DateTime(str_replace('/','-',$last_date));
			
			if($f_last_date < $f_date) $date = $last_date;

			$filter = array();
			
			foreach ($data as $evento) {
				if($evento['DATE'] == $date)
					array_push($filter , $evento);				
			}
			
			if( count ($filter) <= 0) {
				foreach ($data as $evento) {
					if($evento['DATE'] == $default_date)
						array_push($filter , $evento);				
				}	
			}
			
			return $filter;
		}
		
		private function filterById($data , $id = -1) { 
			$filter = array();
			$i = 0;
			foreach ($data as $evento) {
				if($i == $id) {
					array_push($filter , $evento);	
					break;
				}
				$i++;
			}			
			return (! count($filter) ) ? null : $filter;
		}
		
		public function eco_programa ( $atts, $content = null ){
			self::register_styles();
			
			extract(
				shortcode_atts(array(
					'programa_url' => 'https://docs.google.com/spreadsheet/pub?key=0Au0m6p_4_7q6dGZBSlJzVHU0MkxpV2lNUnJwMFZuZVE&single=true&gid=1&output=csv' ,
					'all' => 'false' ,
					'default_date' => '6/10/2014' ,
					'last_date' => '8/10/2014' ,
					'lan' => (isset($atts['lan']) && $atts['lan'] != '')?$atts['lan']:'ES',
					'more' => 'MAS INFORMACIÓN' 				
					), $atts));
			
			ob_start();	
			$data = $this->filter($this->feedToArray($programa_url) , $default_date, $last_date , date('d/m/Y') , $all ); 			
			?>
			<div class="list_programa">
				<div class="toggles">
					<? $i = 0; foreach ($data as $evento) : ?>
					<div class="toggle accent-color <?=(!$i)?'':''?>">
						<h3>
							<a href="#">
								<i class="icon-minus-sign"></i>
								<?=$this->display_evento($evento , $lan , $more , $i , true);?>
							</a>
						</h3>
						<div style="display: <?=(!$i)?'none':'none'?>;">
							<div class="wpb_text_column wpb_content_element ">
								<div class="wpb_wrapper">
									<?=$this->display_evento_detalle($evento , $lan)?>
								</div> 
							</div> 
						</div>
					</div>	
					<? $i++; endforeach; ?>
				</div>				
			</div>
			<?
			$ob_contents = ob_get_contents();
			ob_end_clean();
			return $ob_contents;
		}
		
		// -----------------------------------------
		// -----------------------------------------
		// -----------------------------------------
		
		private function display_evento($evento , $lan , $more = '' , $i = 0 ,  $detalle = false) {
			$sede = $evento['SEDE_'.$lan];
			$como_llegar = $evento['URL_COMO_LLEGAR'];
			$direccion = $evento['DIRECCION_'.$lan];
			$hr = $evento['HR'];
			$dia = $evento['DIA_'.$lan];
			$titulo_evento = $evento['TITULO_EVENTO_'.$lan];			
			$sala = $evento['SALA'];
			$notas = $evento['NOTAS_'.$lan];
			$url_img_portada = $evento['URL_IMG_PORTADA_'.$lan];
			$url_img_sede = $evento['URL_IMG_SEDE_'.$lan];
			$url_detalle = $evento['URL_DETALLE_'.$lan];
			
			$label_sede = ($lan == "ES")?"Sede":"Venue";
			$label_dir = ($lan == "ES")?"Dirección":"Address";
			$label_sala = ($lan == "ES")?"Sala":"Theater";
			ob_start();	
			?>
				<div class="evento" style="display:block;">					
					<!-- begin : programa-item-columns -->
					<div class="wpb_row vc_row-fluid standard_section    " style="padding-top: 0px; padding-bottom: 0px; ">
						<div class="col span_12 dark ">
							<div class="vc_span12 wpb_column column_container col no-extra-padding" data-animation="" data-delay="0">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element ">
										<div class="wpb_wrapper">
											<!-- begin : programa-item-column-izq -->
											<div class="col span_4 text-center" data-animation="" data-delay="0">
												<span class="prog-h1"><?=$titulo_evento?></span><br/>
												<span class="prog-date"><?=$dia?></span><br/>
												<span class="prog-cont-alt"><?=$notas?></span>	
											</div>
											<!-- end : programa-item-column-izq -->
											<!-- begin : programa-item-column-cen -->
											<div class="col span_4 text-center" data-animation="" data-delay="0">
												<span class="prog-cont"><?=($sede!="")?"".$label_sede.": ".$sede."<br/>":""?></span>												
												<span class="prog-time"><?=($hr!="")?$hr."<br/>":""?></span>
												<span class="prog-cont"><?=($sala!="")?"".$label_sala.": ".$sala:""?></span>
											</div>
											<!-- end : programa-item-column-cen -->
											<!-- begin : programa-item-column-der -->
											<div class="col span_4 centered-text col_last" data-animation="" data-delay="0">
												<div class="img-with-aniamtion-wrap center">
													<img class="img-with-animation " data-delay="" data-animation="fade-in" src="<?=$url_img_sede?>" alt="" style="opacity: 1; max-height: 100px;">
												</div>												
											</div>
											<!-- end : programa-item-column-der -->
											<div class="clear"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end : programa-item-columns -->
				</div>				
			<?
			$ob_contents = ob_get_contents();
			ob_end_clean();	
			return $ob_contents;		
		}
		
		private function display_evento_detalle($evento , $lan)
		{
			ob_start();	
			?><div class="detail"><?
			switch ($evento['TIPO']) {
				case 'PROYECCION':
					echo $this->display_evento_detalle_proyeccion($evento , $lan);
					break;
				case 'CONFERENCIA': 
				case 'EVENTO': 
					echo $this->display_evento_detalle_evento($evento , $lan);
					break;
			}
			?></div><?
			$ob_contents = ob_get_contents();
			ob_end_clean();	
			return $ob_contents;			
		}
		
		private function display_evento_detalle_proyeccion($evento , $lan)
		{
			$descripcion_evento = $evento['DESCRIPCION_EVENTO_'.$lan];
			$data = $this->feedToArray($evento['URL_DATA_DETALLE']);
			
			$label_como_llegar = ($lan == "ES")?"Cómo llegar":"Navigate";
			
			$data_counter = 1;
			ob_start();
			?>
			<div class="wpb_row vc_row-fluid standard_section prog-details">
				<div class="col span_6 text-center" style="padding: 5% 0">					
					<div class="prog-details-title" style="padding: 5% 0"><?=$descripcion_evento?></div>
				</div>
				<div class="col span_6 col_last text-center">	
					<div class="img-with-animation-wrap center"><img class="img-with-animation img-prog-cl" data-delay="0" data-animation="fade-in" src="<?=$evento['URL_IMG_PORTADA_'.$lan]?>" alt=""></div>									
					<p style="padding-top: 1em;"><a class="nectar-button small extra-color-1" target="_blank" href="<?=$evento['URL_COMO_LLEGAR']?>" data-color-override="false"><span><?=$label_como_llegar?></span> </a></p>
				</div>
			</div>	
			<div class="wpb_row vc_row-fluid standard_section " style="padding-top: 0px; padding-bottom: 0px; ">
				<div class="col span_12  ">
					<div class="vc_span12 wpb_column column_container col no-extra-padding" data-animation="" data-delay="0">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element ">
								<div class="wpb_wrapper">
								<?foreach ($data as $evento) :
									$director = $evento['Director'];
									$categoria = $evento['Categoria_'.$lan];
									$nacionalidad = $evento['Nacionalidad_'.$lan];
									$anno = $evento['Anno'];
									$titulo = $evento['Titulo_'.$lan];
									$sinopsis = $evento['Sinopsis_'.$lan];
									$still = $evento['Host'].$evento['Path'].$evento['File'];	
									
									$label_director = "Director";								
								?>
							
								<div class="col span_12<?=($data_counter%1==0)?" col_last":""?>" data-animation="" data-delay="0">									
									<div class="row prog-row prog-cont-izq" style="background-position: -<?=rand(30, 80)?>% <?=rand(-9, 80)?>%;">
										<div class="col span_4 prog-col">
											<div class="wpb_wrapper text-center">
												<div class="still-container" style="background-image: url('<?=$still?>')"></div>
												<div class="film-title"><?=$titulo?></div>
												<div class="film-cat"><?=$categoria?></div>												
												<div class="prog-connector"></div>
											</div> 
										</div>
										<div class="col span_8 col_last ">
											<div class="film-dir"><?=(!empty($director))?"".$label_director.": ".$director:""?> (<?=$nacionalidad?>)</div>
											<div class="film-sin"><?=$sinopsis?></div>
											<div class="prog-connector-izq"></div>
										</div>
									</div>									
								</div>			
								<?php $data_counter++; ?>
								<?endforeach;?>
								</div>		
							</div>
						</div>		
					</div>
				</div>		
			</div>
			<?
			$ob_contents = ob_get_contents();
			ob_end_clean();	
			return $ob_contents;
		}
		
		private function display_evento_detalle_evento($evento , $lan)
		{			
			$sede = $evento['SEDE_'.$lan];
			$url_como_llegar = $evento['URL_COMO_LLEGAR'];
			$direccion = $evento['DIRECCION_'.$lan];
			$date = $evento['DATE'];
			$dia = $evento['DIA_'.$lan];
			$hr = $evento['HR'];
			$titulo_evento = $evento['TITULO_EVENTO_'.$lan];
			$descripcion_evento = $evento['DESCRIPCION_EVENTO_'.$lan];
			$tipo = $evento['TIPO'];
			$sala = $evento['SALA'];
			$notas = $evento['NOTAS_'.$lan];
			$url_img_portada = $evento['URL_IMG_PORTADA_'.$lan];
			$url_img_sede = $evento['URL_IMG_SEDE_'.$lan];
			$url_detalle = $evento['URL_DETALLE_'.$lan];
			$url_data_detalle = $evento['URL_DATA_DETALLE'];
			ob_start();
			?>
			<div class="wpb_row vc_row-fluid standard_section prog-details">
				<div class="col span_6 text-center" style="padding: 5% 0">					
					<div class="prog-details-title" style="padding: 5% 0"><?=$titulo_evento?></div>
				</div>
				<div class="col span_6 col_last text-center">	
					<div class="img-with-animation-wrap center"><img class="img-with-animation img-prog-cl" data-delay="0" data-animation="fade-in" src="<?=$url_img_portada?>" alt=""></div>									
					<p style="padding-top: 1em;"><a class="nectar-button small extra-color-1" target="_blank" href="<?=$url_como_llegar?>" data-color-override="false"><span>Cómo llegar</span> </a></p>
				</div>
			</div>	
			<div class="wpb_row vc_row-fluid standard_section " style="padding-top: 0px; padding-bottom: 0px; ">
				<div class="col span_12  ">
					<div class="vc_span12 wpb_column column_container col no-extra-padding" data-animation="" data-delay="0">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element ">
								<div class="wpb_wrapper">											
									<div class="col span_12 col_last" data-animation="" data-delay="0">									
										<div class="row prog-row prog-cont-izq" style="background-position: -<?=rand(30, 80)?>% <?=rand(-9, 80)?>%;">
											<div class="col span_4 prog-col">
												<div class="wpb_wrapper text-center">
													<div class="still-container" style="background-image: url('<?=$url_data_detalle?>')"></div>
													<div class="film-title"><?=$titulo_evento?></div>																									
													<div class="prog-connector"></div>
												</div> 
											</div>
											<div class="col span_8 col_last ">
												<div class="film-dir"><?=$titulo_evento?></div>
												<div class="film-sin"><?=$descripcion_evento?></div>
												<div class="prog-connector-izq"></div>
											</div> 
										</div>									
									</div>
								</div>		
							</div>
						</div>		
					</div>
				</div>		
			</div>						
			<?
			$ob_contents = ob_get_contents();
			ob_end_clean();	
			return $ob_contents;
		}
		
		private function display_evento_detalle_error($lan)
		{
			ob_start();	
			
			$msg = ($lan != 'EN')
				? 'SIN RESULTADOS'
				: 'NO RESULTS'
				;			
			?>
				<h1><?=$msg?></h1>
			<?
			
			$ob_contents = ob_get_contents();
			ob_end_clean();	
			return $ob_contents;
			
		}
		
		// -----------------------------------------
		// -----------------------------------------
		// -----------------------------------------
		public function eco_det_programa ( $atts, $content = null ){
			extract(
				shortcode_atts(array(
					'programa_url' => 'https://docs.google.com/spreadsheet/pub?key=0Au0m6p_4_7q6dEloS0FNWld0Y3JtM2lnZzhlWmptV3c&single=true&gid=1&output=csv' ,
					'lan' => (isset($atts['lan']) && $atts['lan'] != '')?$atts['lan']:'ES',
					'more' => 'MAS INFORMACIÓN'
					), $atts));
			ob_start();	
			$evento = $this->filterById($this->feedToArray($programa_url) , isset($_GET['id'])?$_GET['id'] : -1);
			echo ( $evento != null )
				? $this->display_evento_detalle($evento[0] , $lan)
				: $this->display_evento_detalle_error($lan)
				;

			$ob_contents = ob_get_contents();
			ob_end_clean();
			return $ob_contents;
			
		}
		
		
		public function masonry_programa( $atts, $content = null ) 
		{
			self::register_styles();
			self::regiter_script();
			
			ob_start();
			?>
			<div id="programa">		
				<?php for ($i = 1; $i <= 10; $i++): ?>
					<div class="item"><h1><?=$i ?></h1></div>
				<?php endfor; ?>
			</div>
  			<?			
  			$ob_contents = ob_get_contents();
			ob_end_clean();
			//end : object buffer
			return $ob_contents;
		}
		
		public function regiter_script()
		{
			wp_enqueue_script('app_eco_serv_programa', plugins_url('js/ecofilm-programa.js', __FILE__));
		}
		
		public function register_styles()
		{
			wp_enqueue_style( 'css_eco_programa', plugins_url('css/ecofilm-programa.css', __FILE__));			
		}
		
		
		
		
	}
}
?>