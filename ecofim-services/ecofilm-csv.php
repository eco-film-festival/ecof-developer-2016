<?php 
if(!class_exists('WP_Plugin_Ecofim_Service_csv'))
{
	class WP_Plugin_Ecofim_Service_csv
	{
		private function csvToArray($file, $delimiter) 
		{
			if (($handle = fopen($file, 'r')) !== FALSE) { 
				$i = 0; 
				while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) { 
					for ($j = 0; $j < count($lineArray); $j++) { 
						$arr[$i][$j] = $lineArray[$j]; 
					} 
					$i++; 
				} 
				fclose($handle); 
			} 
			return $arr; 
		}	
		
		public function feedToArray($feed)
		{
			$keys = array();
			$newArray = array();

			$data = $this->csvToArray($feed, ',');
			$count = count($data) - 1;
			$labels = array_shift($data);  

			foreach ($labels as $label) {
				$keys[] = $label;
			}			

			$keys[] = 'id';

			for ($i = 0; $i < $count; $i++) {
				$data[$i][] = $i;
			}

			for ($j = 0; $j < $count; $j++) {
				$d = array_combine($keys, $data[$j]);
				//$d = $this->array_combine_min($keys, $data[$j]);
				$newArray[$j] = $d;
			}

			return $newArray;		
		}
		
		public function JsonToArray($service_url)
		{
			$_result = json_decode(file_get_contents($service_url));
			return $_result;
		}

		public function array_combine_min($arr1, $arr2) {
    		$count = min(count($arr1), count($arr2));
    		return array_combine(array_slice($arr1, 0, $count), array_slice($arr2, 0, $count));
		}
		

	}
}