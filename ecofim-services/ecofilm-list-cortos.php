<?
require 'ecofilm-csv.php';

if(!class_exists('WP_Plugin_Ecofim_Service_Listado_Corto'))
{
	class WP_Plugin_Ecofim_Service_Listado_Corto extends WP_Plugin_Ecofim_Service_csv
	{
		public function listado_preseleccion( $atts, $content = null ) 
		{
			
			extract(
						shortcode_atts(array(
							'data_url' => 'https://ecofilmfestival.info/Public/Preseleccion',
							'show_console' => (isset($atts['show_console']) && $atts['show_console'] != '')?$atts['show_console']:false,
							'lan' => (isset($atts['lan']) && $atts['lan'] != '')?$atts['lan']:'es'
							), $atts));
							
			$result = $this->JsonToArray($data_url);											
			//begin : object buffer
			ob_start();			
			//begin : tabla
			$items = count($result);
			if($items > 0):
			
			foreach( $result as $key=>$value ): 
			
					$titulo = ($lan != 'en' || false)? 'Titulo_ES' : 'Titulo_EN' ;
					$categoria = ($lan != 'en' || false)? 'Categoria_ES' : 'Categoria_EN' ;
		  	?>
		  	
		  	<div id="fws_54067544b8061<?=$value->id?>" class="wpb_row vc_row-fluid standard_section    " style="padding-top: 10px; padding-bottom: 10px; border: solid 1px #E4E4E4; margin-bottom: -1px;">	
				<div class="col span_12 dark ">
					<div class="vc_span12 wpb_column column_container col no-extra-padding" data-animation="" data-delay="0">		
						<div class="wpb_wrapper">			
							<div id="fws_54067544b8c4d<?=$value->id?>" class="wpb_row vc_row-fluid standard_section    " style="padding-top: 0px; padding-bottom: 0px; ">
								<div class="col span_12  ">
									<div class="vc_span8 wpb_column column_container col no-extra-padding" data-animation="" data-delay="0">
										<div class="wpb_wrapper">
			
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<h3 style="text-align: center;"><?=$value->$titulo?></h3>
			
												</div> 
											</div> 
										</div> 
									</div> 
									<div class="vc_span4 wpb_column column_container col no-extra-padding" data-animation="" data-delay="0">
										<div class="wpb_wrapper">
			
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<p><i class="icon-tiny icon-film accent-color"></i> <?= ($lan != 'en' || false)? 'Director' : 'Director' ?>: <?=$value->Director[0]?><br>
													<i class="icon-tiny icon-star accent-color"></i> <?= ($lan != 'en' || false)? 'Categoria' : 'Category' ?>: <?=$value->$categoria?></p>			
												</div> 
											</div> 
										</div> 
									</div> 
								</div>
							</div>			
						</div> 
					</div>
				</div>
			</div>			    
			<?endforeach; 

			endif;
			//end : tabla
			//begin : consola
			if($show_console === true)
			{
				echo "<div style='padding: 10px; border: solid thin #c9c9c9; background-color: #FFF; font-family: monospace;'>";
				print_r($atts);	
				echo $show_console;			
				echo "<hr />";
				print_r($result);
				echo "</div>";	
			}
			//end : consola
			$ob_contents = ob_get_contents();
			ob_end_clean();
			//end : object buffer
			return $ob_contents;
		}
		
		public function listado_sel_oficial( $atts, $content = null )
		{
			
			
		}
		
		public function regiter_script()
		{
			
		}
		
		public function register_styles()
		{
			//wp_enqueue_style( 'css_eco_video_galery', plugins_url('css/ecofilm-listado-preseleccion.css', __FILE__));
		}
		
		
	}
}
?>