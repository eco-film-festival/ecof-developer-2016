(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          }

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  };
	// smartresize
	jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

(function ($) {
  console.log( "metro-ready!" );
    var spacer = -0;
		var $container = $('.eco-metro'),
			colWidth = function () {
				var w = $container.width(),
					columnNum = 1,
					columnWidth = 0;
				if (w > 1200) {
					columnNum  = 5;
				} else if (w > 900) {
					columnNum  = 4;
				} else if (w > 600) {
					columnNum  = 3;
				} else if (w > 300) {
					columnNum  = 2;
				}
				columnWidth = Math.floor(w/columnNum);
				$container.find('.item').each(function() {
					var $item = $(this),
						multiplier_w = $item.attr('class').match(/item-w(\d)/),
						multiplier_h = $item.attr('class').match(/item-h(\d)/),
						width = multiplier_w ? columnWidth*multiplier_w[1]+spacer : columnWidth+spacer,
						height = multiplier_h ? columnWidth*multiplier_h[1]*0.5+spacer : columnWidth*0.5+spacer;
					$item.css({
						width: width,
						height: height
					});
				});
				return columnWidth;
			},
			isotope = function () {
				$container.isotope({
					resizable: false,
					itemSelector: '.item',
					masonry: {
						columnWidth: colWidth(),
						gutterWidth: 0
					}
				});

			};
		isotope();
		$(window).smartresize(isotope);

    $("a[rel^='prettyPhotoMetro']").prettyPhoto();


	}(jQuery));
