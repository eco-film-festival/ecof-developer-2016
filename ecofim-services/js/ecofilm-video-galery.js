jQuery(document).ready(function() {
	/* ----------------------------- */
	/* ----------------------------- */
	/* Botones para filtros Masonry  */
	/* ----------------------------- */
	/* ----------------------------- */
	/* ----------- TODOS ----------- */
	jQuery(".button-filter-todo").click(function(){
		NavBarEval(this);
    	jQuery('.article').show();
        jQuery('#overlays').masonry('layout');
	});
	/* ----------- CATEGORIAS ------ */
    jQuery(".button-filter-animacion").click(function(){
    	NavBarEval(this);
    	jQuery('.article').hide();
        jQuery('.masonry-vgc-animacion').show();
        jQuery('#overlays').masonry('layout');
	});

	jQuery(".button-filter-documental").click(function(){
		NavBarEval(this);
    	jQuery('.article').hide();
        jQuery('.masonry-vgc-documental').show();
        jQuery('#overlays').masonry('layout');
	});

    jQuery(".button-filter-ficcion").click(function(){
    	NavBarEval(this);
    	jQuery('.article').hide();
        jQuery('.masonry-vgc-ficcion').show();
        jQuery('#overlays').masonry('layout');
	});

	jQuery(".button-filter-campana").click(function(){
		NavBarEval(this);
        jQuery('.article').hide();
        jQuery('.masonry-vgc-campana').show();
        jQuery('#overlays').masonry('layout');
	});
	/* ------------ AÑOS ----------- */
	jQuery(".button-filter-anio-2011").click(function(){
		NavBarEval(this);
        jQuery('.article').hide();
        jQuery('.masonry-vgc-anio-2011').show();
        jQuery('#overlays').masonry('layout');
	});

	jQuery(".button-filter-anio-2012").click(function(){
		NavBarEval(this);
        jQuery('.article').hide();
        jQuery('.masonry-vgc-anio-2012').show();
        jQuery('#overlays').masonry('layout');
	});

	jQuery(".button-filter-anio-2013").click(function(){
		NavBarEval(this);
        jQuery('.article').hide();
        jQuery('.masonry-vgc-anio-2013').show();
        jQuery('#overlays').masonry('layout');
	});

	jQuery(".button-filter-anio-2014").click(function(){
		NavBarEval(this);
        jQuery('.article').hide();
        jQuery('.masonry-vgc-anio-2014').show();
        jQuery('#overlays').masonry('layout');
	});

	jQuery(".button-filter-anio-2015").click(function(){
		NavBarEval(this);
        jQuery('.article').hide();
        jQuery('.masonry-vgc-anio-2015').show();
        jQuery('#overlays').masonry('layout');
	});

	jQuery("a.button-filter-todo").addClass("active");

});

function NavBarEval(obj){
	   jQuery('a.active').removeClass('active');
       jQuery(obj).addClass("active");
       jQuery('#current-category').text(jQuery(obj).text());
	}
