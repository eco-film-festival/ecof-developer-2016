(function($) {
    console.log( "ready!" );
    
    var $container = $('#programa');
	// initialize
	$container.masonry({
	  itemSelector: '.item'
	});
	
	$('.item').click(function(){
		if($( this ).hasClass( "w2" )) {
			$( this ).removeClass( "w2" );			
		} else {
			$( this ).addClass( "w2" );
		}
		$container.masonry({itemSelector: '.item'});
	})
}(jQuery));
