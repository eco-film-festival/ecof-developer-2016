<?
require 'ecofilm-csv.php';

if(!class_exists('WP_Plugin_Ecofim_Service_Video_Galery')){
	class WP_Plugin_Ecofim_Service_Video_Galery extends WP_Plugin_Ecofim_Service_csv
	{
		private $url_path_swf;
		function __construct() {
			//add_action("wp_enqueue_scripts",array(&$this,'add_dcwss_scripts'));
			$this->url_path_swf = plugins_url('projekktor/swf/StrobeMediaPlayback/', __FILE__);
		}

		function add_dcwss_scripts() {
			//wp_enqueue_script('app_eco_serv_video_galery_projekktor', plugins_url('projekktor/projekktor-1.3.09.min.js', __FILE__));
			//wp_enqueue_script('app_eco_serv_video_galery_projekktor', plugins_url('projekktor/projekktor-1.2.22r204.min.js', __FILE__));
			//wp_enqueue_script('app_eco_serv_video_galery_projekktor', plugins_url('projekktor-1.2.38r332/projekktor-1.2.38r332.min.js', __FILE__));
			//wp_enqueue_style( 'css_eco_video_galery_projekktor_themes', plugins_url('projekktor/themes/maccaco/projekktor.style.css', __FILE__));
			//wp_enqueue_style( 'css_eco_video_galery_projekktor_themes', plugins_url('projekktor-1.2.38r332/theme/maccaco/projekktor.style.css', __FILE__));
		}

		function limpiar_caracteres_especiales($s) {
			$s = ereg_replace("[áàâãª]","a",$s);
			$s = ereg_replace("[ÁÀÂÃ]","A",$s);
			$s = ereg_replace("[éèê]","e",$s);
			$s = ereg_replace("[ÉÈÊ]","E",$s);
			$s = ereg_replace("[íìî]","i",$s);
			$s = ereg_replace("[ÍÌÎ]","I",$s);
			$s = ereg_replace("[óòôõº]","o",$s);
			$s = ereg_replace("[ÓÒÔÕ]","O",$s);
			$s = ereg_replace("[úùû]","u",$s);
			$s = ereg_replace("[ÚÙÛ]","U",$s);
			$s = str_replace(" ","-",$s);
			$s = str_replace("ñ","n",$s);
			$s = str_replace("Ñ","N",$s);
			//para ampliar los caracteres a reemplazar agregar lineas de este tipo:
			//$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
			return $s;
		}


		public function get_lista_corto($lista_url = 'ultimo_inscrito' , $f_cat = null , $f_anno = null , $f_lugar = null  , $f_est = null)
		{

			$data = $this->feedToArray($lista_url);
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_est = array();

			foreach ($data as $corto) {

				if($corto['display'] == 1 ){
					if( empty($a_est)){
						array_push($a_est , array('class' => $corto['estatus_class'] , 'title' => $corto['Estatus']));
					} else{
						$insert = true;
						foreach ($a_est as $cat)
						{
							if( $cat['class'] == $corto['estatus_class'])
							{
								$insert = false;
								break;
							}
						}
						if($insert)
						{
							array_push($a_est , array('class' => $corto['estatus_class'] , 'title' => $corto['Estatus']));
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_cat = array();

			foreach ($data as $corto) {
				if($corto['display'] == 1 ){
					if( empty($a_cat)){
						array_push($a_cat , array('class' => $corto['class'] , 'title' => $corto['Categoria']));
					} else{
						$insert = true;
						foreach ($a_cat as $cat)
						{
							if( $cat['class'] == $corto['class'])
							{
								$insert = false;
								break;
							}
						}
						if($insert)
						{
							array_push($a_cat , array('class' => $corto['class'] , 'title' => $corto['Categoria']));
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_lugar = array();

			foreach ($data as $corto) {
				if($corto['display'] == 1 ){
					if( empty($a_lugar)){
						array_push($a_lugar , array('class' => $corto['Lugar'] , 'title' => $corto['Lugar_Descripcion']));
					} else{
						$insert = true;
						foreach ($a_lugar as $cat)
						{
							if( $cat['class'] == $corto['Lugar'])
							{
								$insert = false;
								break;
							}
						}
						if($insert)
						{
							array_push($a_lugar , array('class' => $corto['Lugar'] , 'title' => $corto['Lugar_Descripcion']));
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_anno = array();

			foreach ($data as $corto) {
				if($corto['display'] == 1 ){
					if( empty($a_anno)){
						array_push($a_anno , array('class' => $corto['Año'] , 'title' => $corto['Año']));
					} else{
						$insert = true;
						foreach ($a_anno as $cat)
						{
							if( $cat['class'] == $corto['Año'])
							{
								$insert = false;
								break;
							}
						}
						if($insert)
						{
							array_push($a_anno , array('class' => $corto['Año'] , 'title' => $corto['Año']));
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$data_filter = array();

			foreach ($data as $corto) {

				$insert =
				(
					( $f_cat == $corto['class'] || ! $f_cat )
					&& ( $f_anno == $corto['Año'] || ! $f_anno )
					&& ( $f_lugar == $corto['Lugar'] || ! $f_lugar )
					//&& ( $f_est == $corto['estatus_class'] || ! $f_est )
					)
				|| ( ! $f_cat && ! $f_anno && ! $f_lugar && ! $f_est)
				;

				if($insert && $corto['display'] == 1)
				{
					array_push($data_filter , $corto);
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$result = array(
				'f_cat' => $f_cat ,
				'f_anno' => $f_anno ,
				'f_lugar' => $f_lugar ,
				'data' => $data ,
				'lista_url' => $lista_url ,
				'estatus' => $a_est ,
				'cat' => $a_cat,
				'lugar' => $a_lugar,
				'anno' => $a_anno,
				'result' => $data_filter
				);
			return $result;
		}

		public function listado_video_galery( $atts, $content = null )
		{
			self::register_styles();
			self::regiter_script();

			extract(
				shortcode_atts(array(
					//'lista_url' => 'https://docs.google.com/spreadsheets/d/146sT6_D4m2OmeImPafucxI3Y8C2sQekOeKKgnaBV2N4/pub?gid=0&single=true&output=csv' ,
					'lista_url' => '' ,
					'active_filter_categoria' => '' ,
					'active_filter_anno' => '' ,
					'active_filter_lugar' => '' ,
					'active_filter_estatus' => '' ,
					), $atts));-
			//-----------------------------------------
			//-----------------------------------------
			//wp_enqueue_style ( 'css_eco_video_galery_projekktor_themes' );
			//wp_enqueue_style ( 'css_eco_video_galery' );
			//-----------------------------------------
			//-----------------------------------------
			$_GET['c'] = ($active_filter_categoria != '') ? $active_filter_categoria : $_GET['c'];
			$_GET['y'] = ($active_filter_anno != '') ? $active_filter_anno : $_GET['y'];
			$_GET['pl'] = ($active_filter_lugar != '') ? $active_filter_lugar : $_GET['pl'];
			$_GET['es'] = ($active_filter_estatus != '') ? $active_filter_estatus : $_GET['es'];

			$active_filter = (
				$active_filter_categoria != '' ||
				$active_filter_anno != '' ||
				$active_filter_lugar != '' ||
				$active_filter_estatus != ''
				);

			//-----------------------------------------
			//-----------------------------------------
			$lista_url = $lista_url != null ? $lista_url : 'http://ecofilmfestival.info/api/PublicService/VideoGaleryAll';

			$f_cat = (isset($_GET['c']) && $_GET['c'] != '')?$_GET['c']:null;
			$f_anno = (isset($_GET['y']) && $_GET['y'] != '')?$_GET['y']:null;
			$f_lugar = (isset($_GET['pl']) && $_GET['pl'] != '')?$_GET['pl']:null;
			$f_est = (isset($_GET['es']) && $_GET['es'] != '')?$_GET['es']:null; // estatus
			//-----------------------------------------
			$_result = json_decode(file_get_contents($lista_url));//$this->get_lista_corto( $lista_url , $f_cat , $f_anno , $f_lugar , $f_est);
			//-----------------------------------------
			ob_start();
			//-----------------------------------------
			?>
			<? if ( ! $active_filter ) :?>
			<!--<div id="masonry-vg-filters">
				<button class="button-filter-todo">Todo</button>
				<button class="button-filter-animacion">Animación</button>
				<button class="button-filter-documental">Documental</button>
				<button class="button-filter-ficcion">Ficción</button>
				<button class="button-filter-campana">Campaña</button>
				<button class="button-filter-anio-2011">2011</button>
				<button class="button-filter-anio-2012">2012</button>
				<button class="button-filter-anio-2013">2013</button>
			</div>-->
			<div style="clear:both; "></div>
		<? endif; ?>
		<div id="overlays" class="js-masonry"
		data-masonry-options='{ "columnWidth": 300 , "itemSelector": ".overlay" , "gutter": 10 , "isFitWidth": true }'>
		<?foreach( $_result as $_c ):


		//$host = $_c->['host'];
		$host = 'http://ecofilmfestival.org';
		$directory = $_c->directory;
		$hots_img = $_c->host_img;
		$directory_img = $_c->directory_imagen;

		$video_path = $host . $directory;

		$mp4 = $video_path . $_c->mp4_sd;
		$webm = $video_path . $_c->webm;
		$ogg = $video_path . $_c->ogv;

		$poster = $hots_img . $directory_img . $_c->Poster;
		$detail = 'http://ecofilmfestival.org/inscripcion/detalle/';
		$active_detail = 0;

		//$read_only = ( $_c['read-only'] == 0 );
		$read_only = true;

		// $Nombre = ($_GET['lang'] != 'en' || false)?'Nombre':'Nombre_EN';
		$Nombre = ($_GET['lang'] != 'en' || false)?$_c->Nombre:$_c->Nobre_EN;
		$Lugar_Descripcion = ($_GET['lang'] != 'en' || false)?$_c->Lugar_Descripcion:$_c->Lugar_Descripcion_EN;
		$Nacionalidad = ($_GET['lang'] != 'en' || false)?$_c->Nacionalidad:$_c->Nacionalidad_EN;
		$Categoria = ($_GET['lang'] != 'en' || false)?$_c->Categoria:$_c->Categoria_EN;
		$Youtube = ($_GET['lang'] != 'en' || false)?$_c->Youtube:$_c->Youtube_EN;
		$Sinopsis = ($_GET['lang'] != 'en' || false)?$_c->Sinopsis:$_c->Sinopsis_EN;
		$_class = $_c->_class;
		$year = $_c->Año;
		?>
		<div class="post article vg-item overlay <?='masonry-vgc-'.$_class?> <?='masonry-vgc-anio-'.$year?>">
			<div class="post-content-masonry">
				<div class="content-inner">
					<span class="post-featured-img">
						<?if( $read_only ) :?>
						<div style="position: relative;">
							<div class="flowplayer is-splash no-time" style="width: 100%; height: 158px; background:#000000 url(<?=$poster?>) no-repeat 0px 0px/280px 158px;">
								<video preload="none">
									<source type="video/webm" src="<?=$webm?>">
									<source type="video/mp4"  src="<?=$mp4?>">
									<source type="video/ogg" src="<?=$ogg?>">
								</video>
										</div>
										<div class="lugar-<?=$_c->Lugar?>"></div>
									</div>
									<?else :?>
									<div style="position: relative;">
										<?if($active_detail): ?><a href="<?=$detail?>"><?endif;?>
										<img src="<?=$poster?>" class="flow-novid-img" alt="be-my-guest-2-small" title="<?=$Nombre?>">
										<?if($active_detail): ?></a><?endif;?>
										<div class="lugar-<?=$_c->Lugar?>"></div>
									</div>
									<?endif;?>
								</span>
								<div class="post-header">
									<h2 class="title <?='vg-header-'.$_class?>"> <?=$Nombre?><span><?=$Lugar_Descripcion?></span></h2>
									<span class="meta-author"> <?=$_c->Director?> </span>
									<span class="meta-category">| <?=$Categoria?></span>
									<span class="meta-category">| <?=$Nacionalidad?></span>
									<span class="meta-category">| <?=$year?></span>
									<span class="meta-category">| <?=$_c->Duracion?></span>
								</div>
								<? if ($_c->active_sinopsis == 1) :?>
								<p style="text-align: justify; font-size: small; color: #3C3C3C; font-family:Arial, Helvetica, sans-serif;"><?=$Sinopsis?></p>
							<? endif; ?>
						</div>
						<div class="post-meta">
							<div class="vg-list-icon" style="display: block;">
								<?if( $active_detail ): ?>
								<a href="<?=$detail?>" target="_blank"><span class="vg-ficha-tec"></span></a>
								<?endif;?>
								<? if ( $Youtube != '' && $read_only) :?>
								<a href="https://www.youtube.com/watch?v=<?=$Youtube?>" target="_blank"><span class="vg-icon-youtube"></span></a>
							<? endif; ?>
							<? if ( $_c->Vimeo != '' && $read_only) :?>
							<a href="http://vimeo.com/<?=$_c->Vimeo?>" target="_blank"><span class="vg-icon-vimeo"></span></a>
						<? endif; ?>
						<? if ( $_c->Web_site != '' && $read_only) :?>
						<a href="<?=$_c->Web_site?>" target="_blank"><span class="vg-icon-web"></span></a>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?endforeach; ?>
</div>
<script>
// The smallest jQuery Overlay
	$.fn.overlay = function() {

		var ACTIVE = "is-active";

		function toggle(el) {
			$("body").toggleClass("is-overlayed", !!el);

			if (el) {
				el.addClass(ACTIVE).trigger("open");
			} else {
				els.filter("." + ACTIVE).removeClass(ACTIVE).trigger("close");
			}
		}

   // trigger elements
		var els = this.click(function () {
			toggle($(this));
		});

   // esc key
		$(document).keydown(function (e) {
			if (e.which == 27) {
				toggle();
			}
		});

   // close
		$(".close", this).click(function (e) {
			toggle();
			e.stopPropagation();
		});

		return els;
	};

	$(function () {
   // the player will fill the entire overlay
   // so we only need an overlay where inline video is supported
		if (flowplayer.support.inlineVideo) {

      // construct overlays
			$(".overlay").overlay().bind("close", function() {

         // when overlay closes -> unload flowplayer
				$(this).find(".flowplayer").data("flowplayer").unload();

			});
		}
	});

</script>
<?
			//-----------------------------------------
if(isset($_GET['cnl']) && $_GET['cnl'] == 1)
{
	echo '<pre>';
	print_r($atts);
	echo('<hr/>');
	print_r($_result);
	echo '</pre>';
}
			//-----------------------------------------

$list_markup = ob_get_contents();
ob_end_clean();
return $list_markup;
}

		//-------------------------------------BEGIN : SELECCION OFICIAL
public function listado_seloficial_galery( $atts, $content = null )
{
	self::register_styles();
	self::regiter_script();

	extract(
		shortcode_atts(array(
			'lista_url' => 'https://ecofilmfestival.info/Public/SelOficial' ,
			'active_filter_categoria' => '' ,
			'active_filter_anno' => '' ,
			'active_filter_lugar' => '' ,
			'active_filter_estatus' => '' ,
			), $atts));
					//-----------------------------------------
					//-----------------------------------------
	wp_enqueue_style ( 'css_eco_video_galery_projekktor_themes' );
	wp_enqueue_style ( 'css_eco_video_galery' );
					//-----------------------------------------
					//-----------------------------------------
	$_GET['c'] = ($active_filter_categoria != '') ? $active_filter_categoria : $_GET['c'];
	$_GET['y'] = ($active_filter_anno != '') ? $active_filter_anno : $_GET['y'];
	$_GET['pl'] = ($active_filter_lugar != '') ? $active_filter_lugar : $_GET['pl'];
	$_GET['es'] = ($active_filter_estatus != '') ? $active_filter_estatus : $_GET['es'];

	$active_filter = (
		$active_filter_categoria != '' ||
		$active_filter_anno != '' ||
		$active_filter_lugar != '' ||
		$active_filter_estatus != ''
		);

					//-----------------------------------------
					//-----------------------------------------
	$f_cat = (isset($_GET['c']) && $_GET['c'] != '')?$_GET['c']:null;
	$f_anno = (isset($_GET['y']) && $_GET['y'] != '')?$_GET['y']:null;
	$f_lugar = (isset($_GET['pl']) && $_GET['pl'] != '')?$_GET['pl']:null;
					$f_est = (isset($_GET['es']) && $_GET['es'] != '')?$_GET['es']:null; // estatus
					//-----------------------------------------
					$_result = json_decode(file_get_contents($lista_url));//$this->get_lista_corto( $lista_url , $f_cat , $f_anno , $f_lugar , $f_est);
					//-----------------------------------------
					ob_start();
					//-----------------------------------------
					?>
					<? if ( ! $active_filter  && false) :?>
					<div id="masonry-vg-filters">
						<button class="button-filter-todo">Todo</button>
						<button class="button-filter-animacion">Animación</button>
						<button class="button-filter-documental">Documental</button>
						<button class="button-filter-ficcion">Ficción</button>
						<button class="button-filter-campana">Campaña</button>
					</div>
					<div style="clear:both; "></div>
				<? endif; ?>
				<div id="overlays" class="js-masonry"
				data-masonry-options='{ "columnWidth": 300 , "itemSelector": ".overlay" , "gutter": 10 , "isFitWidth": true }'>
				<?foreach($_result as $_c):

				//$host = $_c['host'];
				//$directory = $_c['directory'];
				$hots_img = "https://ecofilmfestival.info";
				$directory_img = $_c->StillPath;

				$poster = $hots_img.$directory_img.$_c->Still;
				$detail = "detail";//$_c['detail'].'?'.$_c['var_detail'].'='.$_c['id_corto'];
				$active_detail = false;//$_c['active_detail'];

				$read_only = false;


				$Nombre = ($_GET['lang'] != 'en' || false)?$_c->Titulo_ES:$_c->Titulo_EN;
				$Lugar_Descripcion = ($_GET['lang'] != 'en' || false)?$_c->Categoria_ES:$_c->Categoria_EN;
				$Nacionalidad = ($_GET['lang'] != 'en' || false)?$_c->Nacionalidad_ES:$_c->Nacionalidad_EN;
				$Categoria = ($_GET['lang'] != 'en' || false)?$_c->Categoria_ES:$_c->Categoria_EN;
				$Youtube = "";//($_GET['lang'] != 'en' || false)?'Youtube':'Youtube_EN';
				$Sinopsis = ($_GET['lang'] != 'en' || false)?$_c->Sinopsis_ES:$_c->Sinopsis_EN;
				$active_sinopsis = 0;


				switch ($_c->Categoria_ES) {
					case 'DOCUMENTAL': $_class = 'documental'; break;
					case 'CAMPAÑA AUDIOVISUAL': $_class = 'campana'; break;
					case 'ANIMACIÓN': $_class = 'animacion'; break;
					case 'FICCIÓN': $_class = 'ficcion'; break;
					default:
					$_class = '';
					break;
				}
				?>
				<div class="post article vg-item overlay <?='masonry-vgc-'.$_class?> <?='masonry-vgc-anio-'.$_c->Anno?>">
					<div class="post-content-masonry">
						<div class="content-inner">
							<span class="post-featured-img">
								<?if( $read_only ) :?>
								<div style="position: relative;">
									<div class="flowplayer is-splash" style="width: 100%; height: 158px; background:#000000 url(<?=$poster?>) no-repeat 0px 0px/280px 158px;">
										<video preload="none">
											<source type="video/webm" src="<?=$webm?>">
												<source type="video/mp4"  src="<?=$mp4?>">
													<source type="video/ogg" src="<?=$ogg?>">
													</video>
												</div>
											</div>
											<?else :?>
											<div style="position: relative;">
												<?if($active_detail): ?><a href="<?=$detail?>"><?endif;?>
												<img src="<?=$poster?>" class="flow-novid-img" alt="be-my-guest-2-small" title="<?=$_c->Titulo_ES?>">
												<?if($active_detail): ?></a><?endif;?>
											</div>
											<?endif;?>
										</span>
										<div class="post-header">
											<h2 class="title vg-header-<?=$_class?>"> <?=$Nombre?><span><?=$Lugar_Descripcion?></span></h2>
											<span class="meta-author"> <?=$_c->Director[0]?> </span>
											<span class="meta-category">| <?=$Categoria?></span>
											<span class="meta-category">| <?=$Nacionalidad?></span>
											<span class="meta-category">| <?=$_c->Anno?></span>
											<!--<span class="meta-category">| <?="duracion"?></span>-->
										</div>
										<? if ($active_sinopsis == 1) :?>
										<p style="text-align: justify; font-size: small; color: #3C3C3C; font-family:Arial, Helvetica, sans-serif;"><?=$Sinopsis?></p>
									<? endif; ?>
								</div>
								<div class="post-meta">
									<div class="vg-list-icon" style="display: block;">
										<?if( $active_detail ): ?>
										<!--<a href="<?=$detail?>" target="_blank"><span class="vg-ficha-tec"></span></a>-->
										<?endif;?>
										<? if ( $Youtube != '' && $read_only) :?>
										<a href="https://www.youtube.com/watch?v=<?=$Youtube?>" target="_blank"><span class="vg-icon-youtube"></span></a>
									<? endif; ?>
									<? if ( $Vimeo != '' && $read_only) :?>
									<a href="http://vimeo.com/<?=$Vimeo?>" target="_blank"><span class="vg-icon-vimeo"></span></a>
								<? endif; ?>
								<? if ( $Web_site != '' && $read_only) :?>
								<a href="<?=$Web_site?>" target="_blank"><span class="vg-icon-web"></span></a>
							<? endif; ?>
						</div>
					</div>
				</div>
			</div>
			<?endforeach; ?>
		</div>
		<script>
		// The smallest jQuery Overlay
			$.fn.overlay = function() {

				var ACTIVE = "is-active";

				function toggle(el) {
					$("body").toggleClass("is-overlayed", !!el);

					if (el) {
						el.addClass(ACTIVE).trigger("open");
					} else {
						els.filter("." + ACTIVE).removeClass(ACTIVE).trigger("close");
					}
				}

		   // trigger elements
				var els = this.click(function () {
					toggle($(this));
				});

		   // esc key
				$(document).keydown(function (e) {
					if (e.which == 27) {
						toggle();
					}
				});

		   // close
				$(".close", this).click(function (e) {
					toggle();
					e.stopPropagation();
				});

				return els;
			};

			$(function () {
		   // the player will fill the entire overlay
		   // so we only need an overlay where inline video is supported
				if (flowplayer.support.inlineVideo) {

		      // construct overlays
					$(".overlay").overlay().bind("close", function() {

		         // when overlay closes -> unload flowplayer
						$(this).find(".flowplayer").data("flowplayer").unload();

					});
				}
			});

		</script>
		<?
					//-----------------------------------------

		// echo '<pre>';
		// print_r($_result);
		// echo '</pre>';

					//-----------------------------------------

		$list_markup = ob_get_contents();
		ob_end_clean();
		return $list_markup;
	}
		//-------------------------------------END : SELECCION OFICIAL

	public function regiter_script(){
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script('app_eco_serv_video_galery_projekktor', plugins_url('flowplayer-5.4.6/flowplayer.js', __FILE__));
		wp_enqueue_script('app_eco_serv_video_galery_filters', plugins_url('js/ecofilm-video-galery.js', __FILE__));
	}

	public function register_styles(){
		wp_enqueue_style( 'css_eco_video_galery_projekktor_themes', plugins_url('flowplayer-5.4.6/skin/minimalist.css', __FILE__));
		wp_enqueue_style( 'css_eco_video_galery', plugins_url('css/ecofilm-video-galery.css', __FILE__));
	}
}
}
