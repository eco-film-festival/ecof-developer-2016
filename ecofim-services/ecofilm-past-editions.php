<?


if(!class_exists('WP_Plugin_Ecofim_Service_Past_Editions')){
	class WP_Plugin_Ecofim_Service_Past_Editions extends WP_Plugin_Ecofim_Service_csv
	{
		private $url_path_swf;
		function __construct() {
			//add_action("wp_enqueue_scripts",array(&$this,'add_dcwss_scripts'));
			//$this->url_path_swf = plugins_url('projekktor/swf/StrobeMediaPlayback/', __FILE__);
		}

		function add_dcwss_scripts() {
			//wp_enqueue_script('app_eco_serv_video_galery_projekktor', plugins_url('projekktor/projekktor-1.3.09.min.js', __FILE__));
			//wp_enqueue_script('app_eco_serv_video_galery_projekktor', plugins_url('projekktor/projekktor-1.2.22r204.min.js', __FILE__));
			//wp_enqueue_script('app_eco_serv_video_galery_projekktor', plugins_url('projekktor-1.2.38r332/projekktor-1.2.38r332.min.js', __FILE__));
			//wp_enqueue_style( 'css_eco_video_galery_projekktor_themes', plugins_url('projekktor/themes/maccaco/projekktor.style.css', __FILE__));
			//wp_enqueue_style( 'css_eco_video_galery_projekktor_themes', plugins_url('projekktor-1.2.38r332/theme/maccaco/projekktor.style.css', __FILE__));
		}

		public function list_past_edition( $atts, $content = null )
		{
			self::register_styles();
			self::regiter_script();

			extract(
			shortcode_atts(array(
				'lista_url' => '' ,
				'active_filter_categoria' => '' ,
				'active_filter_anno' => '' ,
				'active_filter_lugar' => '' ,
				'active_filter_estatus' => '' ,
			), $atts));-


			$lista_url = ($lista_url != null && !empty($_GET["y"])) ?
				$lista_url :
				( (isset($_GET["y"]) && !empty($_GET["y"]) ) ? 'http://ecofilmfestival.info/api/PublicService/EditionByEdition/' . $_GET["y"] : 'http://ecofilmfestival.info/api/PublicService/Edition/' ) ;
			$content = file_get_contents($lista_url);
			$_result = json_decode($content);

			$Id = $_result->Id;
			$Nombre = ($_GET['lang'] != 'en' || false)?$_result->Nombre_ES:$_result->Nombre_EN;
			$Tema = ($_GET['lang'] != 'en' || false)?$_result->Tema_ES:$_result->Tema_EN;
			$Info = ($_GET['lang'] != 'en' || false)?$_result->Info_ES:$_result->Info_EN;
			$BasesConvocatoria = ($_GET['lang'] != 'en' || false)?$_result->BasesConvocatoria_ES:$_result->BasesConvocatoria_EN;
			$BasesConvocatoriaLabel = ($_GET['lang'] != 'en' || false)?"Bases de Convocatoria":"Call for Entries";
			$Cartel = ($_GET['lang'] != 'en' || false)?$_result->Cartel_ES:$_result->Cartel_EN;
			$CartelLabel = ($_GET['lang'] != 'en' || false)?"Cartel":"Poster";
			$Logotipo = ($_GET['lang'] != 'en' || false)?$_result->Logotipo_ES:$_result->Logotipo_EN;
			$DownloadLabel = ($_GET['lang'] != 'en' || false)?"Descargar":"Download";
			$LogotipoLabel = ($_GET['lang'] != 'en' || false)?"Logotipo":"Logo";
			$Teaser = ($_GET['lang'] != 'en' || false)?$_result->Teaser_ES:$_result->Teaser_EN;
			$Informacion = ($_GET['lang'] != 'en' || false)?$_result->Informacion_ES:$_result->Informacion_EN;
			$Direccion = ($_GET['lang'] != 'en' || false)?$_result->Direccion_ES:$_result->Direccion_EN;
			$MapaContato = $_result->MapaContato;
			$ManualGrafico = ($_GET['lang'] != 'en' || false)?$_result->ManualGrafico_ES:$_result->ManualGrafico_EN;
			$ManualGraficoLabel = ($_GET['lang'] != 'en' || false)?"Manual Gráfico":"Identity Manual";
			$Catalogo = ($_GET['lang'] != 'en' || false)?$_result->Catalogo_ES:$_result->Catalogo_EN;
			$CatalogoLabel = ($_GET['lang'] != 'en' || false)?"Catálogo":"Brief";
			$Jurado = $_result->Jurado;
			$JuradoLabel = ($_GET['lang'] != 'en' || false)?"Jurado":"Juries";

			ob_start();
			?>
			<div class="row">
				<div id="fws_5620d2308a696" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid full-width-section parallax_section   "  style="padding-top: 0px; padding-bottom: 0px; ">
					<div class="row-bg-wrap">
						<div class="row-bg  using-bg-color " style="background-color: #ffffff; "></div>
					</div>
					<div class="col span_12 dark left">

						<div class="vc_col-sm-9 wpb_column column_container col padding-5-percent " data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0" style="">
							<div class="wpb_wrapper">
								<div class="nectar-split-heading animated-in">
									<!-- ini  -->
									<div class="ecofilm-past-editions">

										<div class="row">
											<div class="col span_4 dark">
												<div class="epe-list-group">
													<div class="list-group-item centered-text">
														<img src="<?=$Logotipo?>" alt="Logotipo" class="centered-text epe-edlogo"/>
													</div>
													<a class="list-group-item <?php if(empty($BasesConvocatoria)){echo 'not-active';} ?>" href="<?=$BasesConvocatoria?>" target="_blank"><i class="fa fa-fw fa-flag-o" aria-hidden="true"></i>&nbsp; <?=$BasesConvocatoriaLabel?></a>
													<a class="list-group-item <?php if(empty($Teaser)){echo 'not-active';} ?> pp" href="<?=$Teaser?>"><i class="fa fa-fw fa-film" aria-hidden="true"></i>&nbsp; Teaser</a>
													<a class="list-group-item <?php if(empty($Logotipo)){echo 'not-active';} ?> pp" href="<?=$Logotipo?>"><i class="fa fa-fw fa-picture-o" aria-hidden="true"></i>&nbsp; <?=$LogotipoLabel?></a>
													<a class="list-group-item <?php if(empty($ManualGrafico)){echo 'not-active';} ?>" href="<?=$ManualGrafico?>" target="_blank"><i class="fa fa-fw fa-file-pdf-o" aria-hidden="true"></i>&nbsp; <?=$ManualGraficoLabel?></a>
													<a class="list-group-item <?php if(empty($Catalogo)){echo 'not-active';} ?>" href="<?=$Catalogo?>" target="_blank"><i class="fa fa-fw fa-file-text-o" aria-hidden="true"></i>&nbsp; <?=$CatalogoLabel?></a>
												</div>
											</div>

											<div class="col span_8 dark col_last">
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<h6>
															<span class="epe-txt-color-1">
																<?=$Nombre?>
															</span>
														</h6>
													</div>
												</div>
												<h2 class="epe-title">
													<?= $Tema . '
													<span>('.$Id.')</span>'?>
												</h2>
												<hr class="epe-hr11" style="padding-bottom: 27px;"/>
												<p class="epe-info">
													<?=$Info?>
												</p>
												<hr class="epe-hr11"/>
											</div>

										</div>
									</div>
									<!-- end -->
								</div>

							</div>
						</div>

						<div class="vc_col-sm-3 wpb_column column_container col has-animation col_last" data-using-bg="true" data-bg-cover="true" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="reveal-from-bottom" data-delay="0">
							<div class="column-inner-wrap" style="">
								<div style="background-image: url(&quot;<?=$Cartel?>&quot;); background-color: #eaeaea; transform: translate(0px, 0px); min-height: 520px;" data-bg-cover="true" class="column-inner no-extra-padding epe-brighten">
									<div class="wpb_wrapper epe-poster-container">
										<div class="epe-poster-container-centered bg">
											<?=$CartelLabel?>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div id="fws_576be8d5e06c4" data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section vertically-align-columns  " style="padding-top: 0px; padding-bottom: 0px; visibility: visible; background-color: red;">
    <div class="row-bg-wrap instance-2">
        <div class="row-bg   " style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
    </div>
    <div class="col span_12 dark left">
        <div class="vc_col-sm-6 wpb_column column_container col no-extra-padding instance-3" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0" style="height: 600px;">
            <div class="wpb_wrapper" style="margin-top: 44px; margin-bottom: 44px;">
                <div class="img-with-aniamtion-wrap center">
                    <img class="img-with-animation  animated-in" data-delay="0" height="100%" width="100%" data-animation="fade-in" src="http://ecofilmfestival.org/wp-content/uploads/2016/04/logo-ecoff2016_512x512-color-02.png" alt="" style="opacity: 1;">
                    </div>
                </div>
            </div>
            <div class="vc_col-sm-6 wpb_column column_container col no-extra-padding instance-4" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0" style="height: 600px;">
                <div class="wpb_wrapper" style="margin-top: 0px; margin-bottom: 0px;">
                    <div class="wpb_gallery wpb_content_element clearfix">
                        <div class="wpb_wrapper">
                            <div class="wpb_gallery_slidesnectarslider_style" data-interval="5">
                                sss
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

				<div id="fws_575a704079df7" class="wpb_row vc_row-fluid full-width-section parallax_section vertically-align-columns  hide"  style=" ">
			    <div class="row-bg-wrap">
			        <div class="row-bg using-image using-bg-color "></div>
			    </div>
			    <div class="col span_12 dark left">
			        <div style="" class="vc_col-sm-12 wpb_column column_container col " data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#f9f9f9" data-bg-opacity="0.97" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0" >
			            <div class="wpb_wrapper">
											<hr class="epe-hr11" style="padding-bottom: 27px;"/>

											<div class="wpb_wrapper tabbed clearfix" data-style="default" data-alignment="left">
													<ul class="wpb_tabs_nav ui-tabs-nav clearfix">
														<li><a href="#tab-1396210176255-5" class="active-tab"><?=$JuradoLabel?> </a></li>
														<li><a href="#tab-1396210195201-9" class="">Another Tab</a></li>
														<li><a href="#tab-1396210195262-7">One More Tab</a></li>
													</ul>


													<div id="tab-sample-tab" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide clearfix" style="visibility: visible; position: relative; opacity: 1; left: 0px; display: block;">

														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper epe-jurados">
																<?foreach( $Jurado as $j ):
																	$Nombre = $j->Descripcion;
																	$Informacion = ($_GET['lang'] != 'en' || false)?$j->Informacion_ES:$j->Informacion_EN;
																	?>
																<div class="centered-text">
				                            <img src="<?=$j->Image?>" class="epe-jurados-img" alt="" />
																		<p class="epe-jurados-nombre">
																			<?=$Nombre?><br />
																			<span><?=$Informacion?></span>
																		</p>
				                        </div>
																<?endforeach; ?>

															</div>
														</div>

													</div>
													<div id="tab-another-tab" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide clearfix" style="visibility: hidden; position: absolute; opacity: 0; left: -9999px; display: none;">

											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.</p>

												</div>
											</div>

													</div>
													<div id="tab-one-more-tab" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide clearfix" style="visibility: hidden; position: absolute; opacity: 0; left: -9999px; display: none;">

											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.</p>

												</div>
											</div>

													</div>
												</div>

			            </div>
			        </div>
			    </div>
			</div>



			</div>
		<?
		if(isset($_GET['cnl']) && $_GET['cnl'] == 1)
		{
			echo '<pre>';
			print_r($atts);
			echo('<hr/>');
			print_r($_result);
			echo '</pre>';
		}

		$list_markup = ob_get_contents();
		ob_end_clean();
		return $list_markup;
	}

	public function regiter_script(){
		// wp_enqueue_script('js_eco_past_editions', plugins_url('../../themes/salient/js/caroufredsel.min.js', __FILE__));
		wp_enqueue_script('js_eco_past_editions', plugins_url('../../themes/salient/js/nectar-slider.js?ver=7.0.1', __FILE__));
		//http://ecofilmfestival.org/wp-content/themes/salient/js/nectar-slider.js?ver=7.0.1
	}

		public function register_styles(){
			wp_enqueue_style( 'css_eco_past_editions', plugins_url('css/ecofilm-past-editions.css', __FILE__));
		}


	}
}
