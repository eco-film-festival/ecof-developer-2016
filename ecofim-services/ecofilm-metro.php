<?
require 'ecofilm-csv.php';

if(!class_exists('WP_Plugin_Ecofim_Service_Metro')){
	class WP_Plugin_Ecofim_Service_Metro extends WP_Plugin_Ecofim_Service_csv
	{
		private $url_path_swf;
		function __construct() {
			//add_action("wp_enqueue_scripts",array(&$this,'add_dcwss_scripts'));
			//$this->url_path_swf = plugins_url('projekktor/swf/StrobeMediaPlayback/', __FILE__);
		}

		private function time_elapsed_string($datetime, $full = false) {
		    $now = new DateTime;
		    $ago = new DateTime($datetime);
		    $diff = $now->diff($ago);

		    $diff->w = floor($diff->d / 7);
		    $diff->d -= $diff->w * 7;

		    $string = array(
		        'y' => 'year',
		        'm' => 'month',
		        'w' => 'week',
		        'd' => 'day',
		        'h' => 'hour',
		        'i' => 'minute',
		        's' => 'second',
		    );
		    foreach ($string as $k => &$v) {
		        if ($diff->$k) {
		            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
		        } else {
		            unset($string[$k]);
		        }
		    }

		    if (!$full) $string = array_slice($string, 0, 1);
		    return $string ? implode(', ', $string) . ' ago' : 'just now';
		}

		public function ecofilm_metro( $atts, $content = null )
		{
			self::register_styles();
			self::regiter_script();

			$lang = (isset($_GET['lang']))?$_GET['lang']:'es';
			$iconsArray = array("flaticon-cloud351","flaticon-cloud352","flaticon-cloud353","flaticon-cloud354","flaticon-cloud355","flaticon-cloud356","flaticon-cloud357","flaticon-cloud358","flaticon-cloud359","flaticon-cloud360","flaticon-cloud361","flaticon-cloud362","flaticon-cloud363","flaticon-cloud364","flaticon-clouds21","flaticon-cloudy54","flaticon-cloudy55","flaticon-cloudy56","flaticon-cloudy57","flaticon-cloudy58","flaticon-compass114","flaticon-compass2","flaticon-compass3","flaticon-compass4","flaticon-degrees2","flaticon-eclipse6","flaticon-fahrenheit7","flaticon-fog1","flaticon-fog22","flaticon-foggy1","flaticon-fog","flaticon-hail8","flaticon-hail","flaticon-half moon","flaticon-hurricane1","flaticon-lighting1","flaticon-lightning32","flaticon-lightning33","flaticon-lightning34","flaticon-lightning35","flaticon-lightning36","flaticon-moon160","flaticon-moon161","flaticon-moon162","flaticon-moon3","flaticon-rain59","flaticon-rain60","flaticon-rain61","flaticon-rain62","flaticon-rain63","flaticon-rain64","flaticon-rain65","flaticon-rain66","flaticon-rain67","flaticon-rain68","flaticon-rain69","flaticon-rain70","flaticon-rain71","flaticon-rain72","flaticon-rain73","flaticon-raindrop4","flaticon-raindrops7","flaticon-rainy1","flaticon-rainy20","flaticon-snowflake","flaticon-storm15","flaticon-storm16","flaticon-storm17","flaticon-storm2","flaticon-storm","flaticon-sun100","flaticon-sun4","flaticon-sunrise8","flaticon-sunset10","flaticon-sunset11","flaticon-thermometer2","flaticon-thermometer63","flaticon-thermometer65","flaticon-thermometer66","flaticon-umbrella58","flaticon-umbrella59","flaticon-wind36","flaticon-wind37","flaticon-wind38","flaticon-wind39","flaticon-wind40","flaticon-wind41","flaticon-wind42","flaticon-wind43","flaticon-wind44","flaticon-wind45");

			$weather_services = array(
				array(
					'service_url' => 'http://api.openweathermap.org/data/2.5/weather?id=3530597&units=metric&lang='.$lang,
					'name' => 'Ciudad de México'
					),
				array(
					'service_url' => 'http://api.openweathermap.org/data/2.5/weather?id=4005539&units=metric&lang='.$lang,
					'name' => 'Guadalajara, Jalisco'
					),
				array(
					'service_url' => 'http://api.openweathermap.org/data/2.5/weather?id=3995465&units=metric&lang='.$lang,
					'name' => 'Monterrey, Nuevo León'
					),
				array(
					'service_url' => 'http://api.openweathermap.org/data/2.5/weather?id=3521081&units=metric&lang='.$lang,
					'name' => 'Puebla, Puebla'
					),
				array(
					'service_url' => 'http://api.openweathermap.org/data/2.5/weather?id=5389489&units=metric&lang='.$lang,
					'name' => 'Sacramento, California'
					)
			);

			$news_services = array(
				array(
					'service_url' => 'https://news.google.com.mx/news/section?pz=1&cf=all&q=cambio%20climatico&siidp=b78f250f06095734494bf768f81c1ac13b3f&ict=ln&num=3&output=rss',
					'name' => 'Google News : Cambio Climatico'
				),
				array(
					'service_url' => 'https://news.google.com.mx/news/section?pz=1&cf=all&q=climate%20change&siidp=b78f250f06095734494bf768f81c1ac13b3f&ict=ln&num=3&output=rss',
					'name' => 'Google News : Cambio Climatico'
				)
			);

			$icon_url = 'http://openweathermap.org/img/w/';


			ob_start();
			?>
			<div class="eco-metro">
				<!-- weather-df -->
				<? foreach ($weather_services as $location) :
					try {
						$location_json = @file_get_contents($location['service_url']);

						if($location_json === FALSE) {
							echo '<!-- $content error -->';
						} else {

						$location_name = $location['name'];
						$location = json_decode($location_json,true);
						$location_temp = number_format((float)$location[main][temp], 1, '.', '');
						$location_temp_min = number_format((float)$location[main][temp_min], 1, '.', '');
						$location_temp_max = number_format((float)$location[main][temp_max], 1, '.', '');
						$location_humidity = number_format((float)$location[main][humidity], 1, '.', '');
				?>
				<!-- weather-location -->
				<figure class="item weather">
					<figcaption>
						<div class="weather-title-1">&nbsp;</div>
						<div class="weather-title"><?= $location_name ?></div>
						<div class="weather-icon-container">
							<img src="<?= $icon_url.$location[weather][0][icon].'.png' ?>" class="eco-weather-icon" />
						</div>
						<div class="weather-val"><span class="weather-val-1"><?= $location_temp ?></span> °C</div>
						<div class="weather-title-2"><?=  strtoupper($location[weather][0][description]) ?></div>
						<div class="weather-title-3"><i class="fa fa-tint"></i>&nbsp;<?=  $location_humidity ?>%&nbsp;</div>
					</figcaption>
				</figure>
				<!--/weather-location -->
				<? }
				} catch (Exception $e) {
					echo '<!-- Excepción capturada: ',  $e->getMessage(), '-->',"\n";
				} ?>
				<? endforeach; ?>
				<?
				//titulos segun idioma
				switch ($lang) {
					case 'en':
						$title1 = "National Weather Service";
						$title2 = "Call for Entries 5th edition";
						$title3 = "Press Conference";
						$title4 = "Winners 4th edition";
						$title5 = "Press Room";
						$title6 = "Teaser Call for Entries 5th edition";
						$sub1 = "SATELLITE";
						$sub2 = "Gallery";
						$sub3 = "Registry";
						$sub4 = "Read More";
						$convocatoria_url = "http://ecofilmfestival.org/convocatoria/?lang=en";
						$convocatoria = "https://vimeo.com/125166739";
						break;

					default:
						$title1 = "Servicio Meteorológico Nacional";
						$title2 = "Convocatoria 5ta edición";
						$title3 = "Cobertura Conferencia de Prensa";
						$title4 = "Ganadores 4ta edición";
						$title5 = "Sala de Prensa";
						$title6 = "Convocatoria 5ta edición";
						$sub1 = "SATÉLITE";
						$sub2 = "Galería";
						$sub3 = "Registro";
						$sub4 = "Leer más";
						$convocatoria_url = "http://ecofilmfestival.org/convocatoria/";
						$convocatoria = "https://vimeo.com/123269706";
						break;
				}

				?>
				<figure class="item item-w2 item-h3">
					<figcaption class="climamx-visible">
						<div class="clima-data">
							<span class="sat-sub2"><?=$title1?></span>
							<span class="sat-sub"	><?=$sub1?></span><span class="sat"> GOES ESTE</span>
						</div>
					</figcaption>
				</figure>
				<figure class="item item-h2">
					<figcaption class="link-base">
						<div class="bg1 link-container1 grayscale" ></div>
						<span class="link-bg1 link-data1"><?=$title2?></span>
						<span class="link-data2"><a href="<?=$convocatoria_url?>" target="_blank"><i class="fa fa-link"></i> <?=$sub4?></span></a>
					</figcaption>
				</figure>
				<figure class="item item-h2">
					<figcaption class="link-base">
						<div class="bg2 link-container1 grayscale" ></div>
						<span class="link-bg1 link-data1"><?=$title3?></span>
						<span class="link-data2"><a href="http://ecofilmfestival.org/galeria-2/<?=($lang=='en')?'?lang='.$lang:''?>" target="_blank"><i class="fa fa-camera"></i> <?=$sub2?></span></a>
					</figcaption>
				</figure>
				<figure class="item">
					<figcaption class="link-base">
						<div class="bg3 link-container1 grayscale" ></div>
						<span class="link-bg1 link-data1"><?=$title4?></span>
						<span class="link-data2"><a href="<?=($lang=='en')?'http://ecofilmfestival.org/2014-solid-waste/?lang='.$lang:'http://ecofilmfestival.org/2014-residuos-solidos/'?>" target="_blank"><i class="fa fa-link"></i> ECOFILM Festival 2014</span></a>
					</figcaption>
				</figure>
				<figure class="item">
					<figcaption class="link-base">
						<div class="bg4 link-container1 grayscale" ></div>
						<span class="link-bg1 link-data1"><?=$title5?></span>
						<span class="link-data2"><a href="http://ecofilmfestival.org/prensa/registro/<?=($lang=='en')?'?lang='.$lang:''?>" target="_blank"><i class="fa fa-link"></i> <?=$sub3?></span></a>
					</figcaption>
				</figure>

				<figure class="item item-w2 item-h3">
					<figcaption class="link-base">
						<div class="bg5 link-container1 grayscale" ></div>
						<span class="link-data1 link-bg1"><?=$title6?></span>
						<span class="link-data2"><a href="<?=$convocatoria?>" rel="prettyPhotoMetro"><i class="fa fa-play-circle"></i> Video</span></a>
					</figcaption>
				</figure>

				<?
				$recent = new WP_Query("showposts=3&cat=105");
				foreach($recent->posts as $post) :
					if (has_post_thumbnail($post->ID)) {
		        $retina  = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'homepage-thumb-retina' );
						//echo '<img src="' . $retina[0] . '" alt="' . the_title() . '" width="24" height="24" />' ;
						$post_title = $post->post_title;
						$post_guid = $post->guid;
				?>
				<figure class="item item-h2">
					<figcaption class="link-base">
						<div class="link-container1 grayscale" style="background-image: url(<?=$retina[0]?>); background-size: inherit; background-color: rgb(29, 29, 29); background-size: cover; background-repeat: no-repeat;" ></div>
						<span class="link-bg4 link-data1"><?= $post_title ?></span>
						<span class="link-data2"><a href="<?= $post_guid ?>" target="_blank"><i class="fa fa-link"></i> <?=$sub4?></span></a>
					</figcaption>
				</figure>
				<?
					};
					endforeach;
				?>

				<?
					try {
						$xml = ($lang == 'en')?simplexml_load_file($news_services[1]['service_url']):simplexml_load_file($news_services[0]['service_url']);
						if ($xml === false) {
						    echo "<!--Failed loading XML: ";
						    foreach(libxml_get_errors() as $error) {
						        echo "<br>", $error->message;
						    }
								echo "-->";
						} else {
				?>
				<!-- google-news -->
				<? foreach ($xml->channel->item as $item) : ?>
				<?php
					list($titulo, $fuente) = split(" - ", $item->title);
				?>
				<figure class="item">
					<figcaption class="link-base link-bg5">
						<!--  begin : eco-not -->
						<div class="eco-not-container">
							<div class="eco-not-icon">
								<i class="<?=$iconsArray[array_rand($iconsArray)]?> ecof-iconsize-1" style="display: block;"></i>
							</div>
						  <div class="eco-not-title">
								<span class="eco-not-fuente"><?=self::time_elapsed_string($item->pubDate)?></span><br/>
								<a href="<?=$item->link?>" target="_blank">
									<?=$titulo?>
								</a>
								<br/><span class="eco-not-fuente"><?=$fuente?></span>
							</div>
						</div>
						<!--  end : eco-not -->
					</figcaption>
				</figure>
				<? endforeach; ?>
				<!--/google-news -->
				<? }
					} catch (Exception $e) {
					echo '<!-- Excepción capturada: ',  $e->getMessage(), '-->',"\n";
				} ?>
			</div>
      <?
			//------------- BEGIN : CONSOLA ----------------------------
			if(isset($_GET['cnl']) && $_GET['cnl'] == 1) {

				echo '<pre>';
				//foreach($recent->posts as $post) {
					/*if (has_post_thumbnail($post->ID)) {
			        $retina  = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'homepage-thumb-retina' );
			        echo '<img src="' . $retina[0] . '" alt="' . the_title() . '" width="24" height="24" />' ;
			    };*/
					print_r($xml->channel);
					//print_r($post->post_title);
				//}
				echo('<hr/>');
				//print_r($recent);
				echo '</pre>';

			}
			//------------- END : CONSOLA ----------------------------
			$eco_container = ob_get_contents();
			ob_end_clean();
			return $eco_container;
		}
		//-------------------------------------END : SELECCION OFICIAL

		public function regiter_script(){
			wp_enqueue_script( 'jquery' );
			//wp_enqueue_script('app_eco_serv_metro_isotope', plugins_url('js/jquery.isotope.min.js', __FILE__));
			wp_enqueue_script('app_eco_serv_metro', plugins_url('js/ecofilm-metro.js', __FILE__));
		}

		public function register_styles(){
			wp_enqueue_style( 'css_eco_serv_metro', plugins_url('css/ecofilm-metro.css', __FILE__));
		}
	}
}
