<?
require 'ecofilm-csv.php';

if(!class_exists('WP_Plugin_Ecofim_Service_Edicion_Player')){
	class WP_Plugin_Ecofim_Service_Edicion_Player extends WP_Plugin_Ecofim_Service_csv
	{
		private $url_path_swf;
		
		public function display_player( $atts, $content = null ){
			
			self::register_styles();
			self::regiter_script();	
			
			extract(
				shortcode_atts(array(
					'lista_url' => 'https://docs.google.com/spreadsheet/pub?key=0Au0m6p_4_7q6dFVybGFfRG1JbmpCTmIyYUNUbExpbnc&output=csv' ,
					'active_filter_categoria' => '' ,
					'active_filter_anno' => '' ,
					'active_filter_lugar' => '' ,
					'active_filter_estatus' => '' ,
					), $atts));
					
			$_GET['c'] = ($active_filter_categoria != '') ? $active_filter_categoria : $_GET['c'];
			$_GET['y'] = ($active_filter_anno != '') ? $active_filter_anno : $_GET['y'];
			$_GET['pl'] = ($active_filter_lugar != '') ? $active_filter_lugar : $_GET['pl'];
			$_GET['es'] = ($active_filter_estatus != '') ? $active_filter_estatus : $_GET['es'];
			
			$active_filter = ( 
				$active_filter_categoria != '' || 
				$active_filter_anno != '' || 
				$active_filter_lugar != '' || 
				$active_filter_estatus != ''
				);
				
			$f_cat = (isset($_GET['c']) && $_GET['c'] != '')?$_GET['c']:null;
			$f_anno = (isset($_GET['y']) && $_GET['y'] != '')?$_GET['y']:null; 
			$f_lugar = (isset($_GET['pl']) && $_GET['pl'] != '')?$_GET['pl']:null;
			$f_est = (isset($_GET['es']) && $_GET['es'] != '')?$_GET['es']:null;
			
			$_result = $this->get_lista_corto( $lista_url , $f_cat , $f_anno , $f_lugar , $f_est);
			//---------- begin : output
			ob_start();
			?>
			
			<div id="content">

				<div id="basic-playlist"
				   class="flowplayer is-splash is-closeable"
				   data-ratio="0.5625">
	
				   <video class="bp-video">			   
				   <source type="video/mp4"
				           src="http://ecofilmfestival.org/video_eco_ed/2014/6098b7ce-8f7f-458d-88d7-3158859ed2a0.mp4">
				   </video>
	
				   <a class="fp-prev"></a>
				   <a class="fp-next"></a>
	
				   <div class="fp-playlist" style="display: none;">
				   	   <?foreach($_result['data'] as $_c):					
						
						$host = $_c["host"];
						$directory = $_c["directory"];
						$mp4_sd = $_c["mp4_sd"];
						
						/*$hots_img = "https://ecofilmfestival.info";
						$directory_img = $_c->StillPath;
						
						$poster = $hots_img.$directory_img.$_c->Still;
						$detail = "detail";//$_c['detail'].'?'.$_c['var_detail'].'='.$_c['id_corto'];
						$active_detail = false;//$_c['active_detail'];
						
						$read_only = false;
						
						
						$Nombre = ($_GET['lang'] != 'en' || false)?$_c->Titulo_ES:$_c->Titulo_EN;
						$Lugar_Descripcion = ($_GET['lang'] != 'en' || false)?$_c->Categoria_ES:$_c->Categoria_EN;
						$Nacionalidad = ($_GET['lang'] != 'en' || false)?$_c->Nacionalidad_ES:$_c->Nacionalidad_EN;
						$Categoria = ($_GET['lang'] != 'en' || false)?$_c->Categoria_ES:$_c->Categoria_EN;
						$Youtube = "";//($_GET['lang'] != 'en' || false)?'Youtube':'Youtube_EN';
						$Sinopsis = ($_GET['lang'] != 'en' || false)?$_c->Sinopsis_ES:$_c->Sinopsis_EN;
						$active_sinopsis = 0;*/					
						
						?>
				      <a class="itemX" href="<?=$host.$directory.$mp4_sd?>"
				         data-cuepoints="[0.5, 1]"></a>
				         
				      <?endforeach?>
				   </div>
	
				</div>

			</div>			
			<!--<code><?php //print_r($_result); ?></code> -->
			<?php
			$list_markup = ob_get_contents();
			ob_end_clean();
			//---------- end : output
			return $list_markup;
		}
		
		public function get_lista_corto($lista_url = 'ultimo_inscrito' , $f_cat = null , $f_anno = null , $f_lugar = null  , $f_est = null)
		{
			
			$data = $this->feedToArray($lista_url); 
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_est = array();
			
			foreach ($data as $corto) {
				
				if($corto['display'] == 1 ){
					if( empty($a_est)){
						array_push($a_est , array('class' => $corto['estatus_class'] , 'title' => $corto['Estatus']));
					} else{
						$insert = true;
						foreach ($a_est as $cat)
						{
							if( $cat['class'] == $corto['estatus_class'])
							{
								$insert = false;
								break;							
							}
						}
						if($insert)
						{
							array_push($a_est , array('class' => $corto['estatus_class'] , 'title' => $corto['Estatus']));	
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_cat = array();
			
			foreach ($data as $corto) {
				if($corto['display'] == 1 ){
					if( empty($a_cat)){
						array_push($a_cat , array('class' => $corto['class'] , 'title' => $corto['Categoria']));
					} else{
						$insert = true;
						foreach ($a_cat as $cat)
						{
							if( $cat['class'] == $corto['class'])
							{
								$insert = false;
								break;							
							}
						}
						if($insert)
						{
							array_push($a_cat , array('class' => $corto['class'] , 'title' => $corto['Categoria']));	
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_lugar = array();
			
			foreach ($data as $corto) {
				if($corto['display'] == 1 ){
					if( empty($a_lugar)){
						array_push($a_lugar , array('class' => $corto['Lugar'] , 'title' => $corto['Lugar_Descripcion']));
					} else{
						$insert = true;
						foreach ($a_lugar as $cat)
						{
							if( $cat['class'] == $corto['Lugar'])
							{
								$insert = false;
								break;							
							}
						}
						if($insert)
						{
							array_push($a_lugar , array('class' => $corto['Lugar'] , 'title' => $corto['Lugar_Descripcion']));	
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$a_anno = array();
			
			foreach ($data as $corto) {
				if($corto['display'] == 1 ){
					if( empty($a_anno)){
						array_push($a_anno , array('class' => $corto['Año'] , 'title' => $corto['Año']));
					} else{
						$insert = true;
						foreach ($a_anno as $cat)
						{
							if( $cat['class'] == $corto['Año'])
							{
								$insert = false;
								break;							
							}
						}
						if($insert)
						{
							array_push($a_anno , array('class' => $corto['Año'] , 'title' => $corto['Año']));	
						}
					}
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$data_filter = array();

			foreach ($data as $corto) {
				
				$insert = 
				( 
					( $f_cat == $corto['class'] || ! $f_cat ) 
					&& ( $f_anno == $corto['Año'] || ! $f_anno ) 
					&& ( $f_lugar == $corto['Lugar'] || ! $f_lugar ) 
					//&& ( $f_est == $corto['estatus_class'] || ! $f_est ) 
					) 
				|| ( ! $f_cat && ! $f_anno && ! $f_lugar && ! $f_est)
				;
				
				if($insert && $corto['display'] == 1)
				{
					array_push($data_filter , $corto);
				}
			}
			// ------------------------------
			// ------------------------------
			// ------------------------------
			$result = array(
				'f_cat' => $f_cat ,
				'f_anno' => $f_anno ,
				'f_lugar' => $f_lugar ,
				'data' => $data ,
				'lista_url' => $lista_url ,
				'estatus' => $a_est ,
				'cat' => $a_cat,
				'lugar' => $a_lugar,
				'anno' => $a_anno,
				'result' => $data_filter
				);
			return $result;
		}
		
		public function regiter_script(){
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script('app_eco_serv_edicion_flowplayer', plugins_url('flowplayer-5.5.2/flowplayer.min.js', __FILE__));
			//wp_enqueue_script('app_eco_serv_edicion_flowplayer_display_player', plugins_url('js/ecofilm-display-player.js', __FILE__));
			//wp_enqueue_script('app_eco_serv_video_galery_filters', plugins_url('js/ecofilm-video-galery.js', __FILE__)); 
		}
		 		
		public function register_styles(){
			wp_enqueue_style( 'css_eco_serv_edicion_flowplayer_themes', plugins_url('flowplayer-5.5.2/skin/minimalist.css', __FILE__));
			wp_enqueue_style( 'css_eco_serv_edicion_flowplayer_basic', plugins_url('flowplayer-5.5.2/playlist/basic.css', __FILE__));
			//wp_enqueue_style( 'css_eco_video_galery', plugins_url('css/ecofilm-video-galery.css', __FILE__));
		}
		
	}
}