<?

if(!class_exists('WP_Plugin_Ecofim_Service_Ganadores'))
{
	class WP_Plugin_Ecofim_Service_Ganadores extends WP_Plugin_Ecofim_Service_csv
	{		
		
		public function eco_ganadores ( $atts, $content = null ){
				
			self::register_styles();
			self::regiter_script();			
			
			ob_start();	
			 			
			?>
			<div class="container-grid">
				<div class="main">
					<ul id="og-grid" class="og-grid">
						<!-- items -->
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/3dd94810-5739-45d0-bb31-26aefcb58f55.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/424.jpg" data-title="¿Basura o recurso?" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/4.jpg" alt="img01"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/331e2e06-3f68-4c80-b1fd-9d31572584ba.jpg" data-title="Conciencia Natural" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/3.jpg" alt="img02"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/957cd605-e4f2-48ea-8513-a13c79b83b05.jpeg" data-title="Náufrago" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/2.jpg" alt="img03"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/424.jpg" data-title="¿Basura o recurso?" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/4.jpg" alt="img01"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/3dd94810-5739-45d0-bb31-26aefcb58f55.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/331e2e06-3f68-4c80-b1fd-9d31572584ba.jpg" data-title="Conciencia Natural" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/3.jpg" alt="img02"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/957cd605-e4f2-48ea-8513-a13c79b83b05.jpeg" data-title="Náufrago" 
							data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/2.jpg" alt="img03"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/424.jpg" data-title="¿Basura o recurso?" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/4.jpg" alt="img01"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/331e2e06-3f68-4c80-b1fd-9d31572584ba.jpg" data-title="Conciencia Natural" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/3.jpg" alt="img02"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/957cd605-e4f2-48ea-8513-a13c79b83b05.jpeg" data-title="Náufrago" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/2.jpg" alt="img03"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/424.jpg" data-title="¿Basura o recurso?" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/4.jpg" alt="img01"/>
							</a>
						</li>
						<li>
							<a href="http://#" data-mp4src="https://ecofilmfestival.info/Uploads/Video/c8630d1f-c18d-4078-a683-2d9b3d98d197.mp4"  
							data-largesrc="https://ecofilmfestival.info/Uploads/331e2e06-3f68-4c80-b1fd-9d31572584ba.jpg" data-title="Conciencia Natural" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
								<img src="http://ecofilmfestival.org/wp-content/uploads/2013/02/3.jpg" alt="img02"/>
							</a>
						</li>
					</ul>
					<!-- /items -->
				</div>
			</div><!-- /container-grid -->
			<script>
			/*$(function() {
				Grid.init();
			});*/
			jQuery(document).ready(function( $ ) {
				
				Grid.init();
				console.log("we're inside!");
				
			});
			</script>
			<?
			$ob_contents = ob_get_contents();
			ob_end_clean();
			return $ob_contents;
		}
		
		public function regiter_script()
		{			
			wp_enqueue_script('app_eco_serv_ganadores', plugins_url('js/ecofilm-ganadores-grid.js', __FILE__));			
		}
		
		public function register_styles()
		{
			wp_enqueue_style( 'css_eco_ganadores', plugins_url('css/ecofilm-ganadores-default.css', __FILE__));
			wp_enqueue_style( 'css_eco_ganadores_cmp', plugins_url('css/ecofilm-ganadores-component.css', __FILE__));			
		}
		
		
		
		
	}
}
?>