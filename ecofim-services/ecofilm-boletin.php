<?
require 'ecofilm-csv.php';

if(!class_exists('WP_Plugin_Ecofim_Service_Boletin'))
{
	class WP_Plugin_Ecofim_Service_Boletin extends WP_Plugin_Ecofim_Service_csv
	{
		//-----------------------------------------
		//-----------------------------------------
		//-----------------------------------------
		public function regiter_script()
		{
			wp_register_script('app_eco_serv_boletin', plugins_url('js/ecofilm-boletin.js', __FILE__),array("jquery"));
		}
		
		public function register_styles()
		{
			wp_register_style('ccs_eco_serv_boletin', plugins_url('css/ecofilm-boletin.css', __FILE__));	
		}
		//-----------------------------------------
		//-----------------------------------------
		//-----------------------------------------
		public function registro_boletin($atts, $content = null)
		{
			extract(
				shortcode_atts(array(
					'action' => 'http://newsletter.ecofilmfestival.org/form.php?form=3',
					'method' => 'post',
					), $atts));  
			
			wp_enqueue_script( 'app_eco_serv_registro_boletin' );
			ob_start();
			// lang=en
			$submit_value = (isset($_GET['lang']) && $_GET['lang'] == 'en')?'Subscribe':'Recibir Boletin';
			
			
			?>
			<form method="<?=$method?>" action="<?=$action?>" id="frmSS3" onsubmit="return CheckForm3(this);">
				<p>
					<span style="color:red;">*</span>Email: <br>
	    			<input type="text" name="email" value="" />
	    			<input type="hidden" name="format" value="h" />
	    			<input type="submit" value="<?=$submit_value?>" />
	    		</p>
			</form>
	
			<script type="text/javascript">
			// <![CDATA[
				function CheckMultiple3(frm, name) {
					for (var i=0; i < frm.length; i++)
					{
						fldObj = frm.elements[i];
						fldId = fldObj.id;
						if (fldId) {
							var fieldnamecheck=fldObj.id.indexOf(name);
							if (fieldnamecheck != -1) {
								if (fldObj.checked) {
									return true;
								}
							}
						}
					}
					return false;
				}
				function CheckForm3(f) {
					var email_re = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
					if (!email_re.test(f.email.value)) {
						alert("Por favor escribe tu Email | Please enter your email address.");
						f.email.focus();
						return false;
					}
					return true;
				}
			// ]]>
			</script>
			<?
			$form_markup = ob_get_contents();
			ob_end_clean();
			return $form_markup;		
		}
		
		public function error_boletin()
		{
			return $_GET['Errors'];
		}
		//-----------------------------------------
		//-----------------------------------------
		//-----------------------------------------
		public function listado_boletin($atts, $content = null)
		{
			extract(
				shortcode_atts(array(
					'lista_url' => 'https://docs.google.com/spreadsheet/pub?key=0Au0m6p_4_7q6dEhlVVRqS1l0M0dEdzZrVzNXbl9zVXc&output=csv',
					'count' => 'all' ,
					'class' => 'listado_boletin' ,
					), $atts));  
			//-----------------------------------------
			//-----------------------------------------
			wp_enqueue_style ( 'ccs_eco_serv_boletin' );
			wp_enqueue_script( 'app_eco_serv_boletin' );
			//-----------------------------------------
			//-----------------------------------------
			ob_start();
			
			$data = $this->feedToArray($lista_url); 		
			$break_count = ((int)$count > 0) ;
			$i = 0;
			?><ul class="<?=$class?>"><?
			foreach ($data as $boletin) {
				if ( $break_count  && $i >= $count){
					break;
				}
				$href = $boletin['Host'].$boletin['Directory'].$boletin['File'];
				?>
				<li class="item col boxed no-extra-padding">
					<a href="<?=$href?>" target="_blank">
						<h3>
							<i class="icon-medium icon-envelope extra-color-1"></i>
							<?=$boletin['Titulo']?>
							<span>
								<?=$boletin['Año']?> | <?=$boletin['Fecha']?> 
							</span>
						</h3>
						<p><?=$boletin['Cometario']?></p>
					</a>					
				</li>				
				<?							
				$i++;
			}
			?></ul><?
			/*
			echo '<pre>';					
			print_r($data);		
			echo '</pre>';		
			*/	
			$list_markup = ob_get_contents();
			ob_end_clean();
			return $list_markup;		
		}
		
		public function ultimos_boletin($atts, $content = null)
		{
			extract(
				shortcode_atts(array(					
					'count' => 1 ,
					), $atts));  
					
			return do_shortcode('[listado_boletin count="'.$count.'"]'.$content.'[/listado_boletin]');	
		}
		//-----------------------------------------
		//-----------------------------------------
		//-----------------------------------------
	}
	
}

?>