<?php
/*
Plugin Name: SERVICIOS ECOFILM
Plugin URI: http://ecofilmfestival.org/
Description: Servicio Ecofilm
Version: 1.2
Author: zowarin@gmail.com
Author URI: http://zowarin.com
License: GPLv2
*/
?>
<?php
// require 'ecofim-services-bussines.php';
require 'ecofilm-list-cortos.php';
require 'ecofilm-boletin.php';
require 'ecofilm-comunicado.php';
require 'ecofilm-video-galery.php';
require 'ecofilm-programa.php';
require 'ecofilm-ganadores.php';
require 'ecofilm-edicion-player.php';
require 'ecofilm-metro.php';
require 'ecofilm-past-editions.php';

if(!class_exists('WP_Plugin_Ecofim_Service'))
{
	class WP_Plugin_Ecofim_Service
	{
		private $boletin = null;
		private $comunicado = null;
		private $video_galery = null;
		private $list_corto = null;
		private $programa = null;
		private $ganadores = null;
		private $past_editions = null;
		//private $bussines = null;
		//

		public function __construct()
		{
			// ----------------------------------
			add_action('wp_enqueue_scripts', array(&$this, 'regiter_script'));
			add_action('wp_enqueue_scripts', array(&$this, 'register_styles'));
			// ----------------------------------
			// --- PRESELECCION Y SEL. OFICIAL
			// ----------------------------------
			$this->list_corto = new WP_Plugin_Ecofim_Service_Listado_Corto();
			add_shortcode('listado_preseleccion' , array(&$this->list_corto , 'listado_preseleccion'));
			add_shortcode('listado_sel_oficial' , array(&$this->list_corto , 'listado_sel_oficial'));
			// ------------------------
			// --- BOLETIN ------------
			// ------------------------
			$this->boletin = new WP_Plugin_Ecofim_Service_Boletin();
			add_shortcode('registro_boletin' , array(&$this->boletin , 'registro_boletin'));
			add_shortcode('listado_boletin' , array(&$this->boletin , 'listado_boletin'));
			add_shortcode('ultimos_boletin' , array(&$this->boletin , 'ultimos_boletin'));
			add_shortcode('error_boletin' , array(&$this->boletin , 'error_boletin'));
			// ------------------------
			// --- COMUNICADOS --------
			// ------------------------
			$this->comunicado = new WP_Plugin_Ecofim_Service_Comunicado();
			add_shortcode('listado_comunicado' , array(&$this->comunicado , 'listado_comunicado'));
			add_shortcode('ultimos_comunicado' , array(&$this->comunicado , 'ultimos_comunicado'));
			// ------------------------
			// --- VIDEO GALERIA ------
			// ------------------------
			$this->video_galery = new WP_Plugin_Ecofim_Service_Video_Galery();
			add_shortcode('listado_video_galery' , array(&$this->video_galery , 'listado_video_galery'));
			add_shortcode('display_video' , array(&$this->video_galery , 'display_video'));
			add_shortcode('listado_seloficial_galery' , array(&$this->video_galery , 'listado_seloficial_galery'));
			// ------------------------
			// --- PROGRAMA ------
			// ------------------------
			$this->programa = new WP_Plugin_Ecofim_Service_Programa();
			add_shortcode('masonry_programa' , array(&$this->programa , 'masonry_programa'));
			add_shortcode('eco_programa' , array(&$this->programa , 'eco_programa'));
			add_shortcode('eco_det_programa' , array(&$this->programa , 'eco_det_programa'));
			// ------------------------
			// --- PROGRAMA ------
			// ------------------------
			$this->ganadores = new WP_Plugin_Ecofim_Service_Ganadores();
			add_shortcode('eco_ganadores' , array(&$this->ganadores , 'eco_ganadores'));
			// ------------------------
			// --- EDICION PLAYER ------
			// ------------------------
			$this->display_player = new WP_Plugin_Ecofim_Service_Edicion_Player();
			add_shortcode('display_player' , array(&$this->display_player , 'display_player'));
			// ------------------------
			// ------------------------
			// --- METRO ------
			// ------------------------
			$this->ecofilm_metro = new WP_Plugin_Ecofim_Service_Metro();
			add_shortcode('ecofilm_metro' , array(&$this->ecofilm_metro , 'ecofilm_metro'));
			// ------------------------
			// --- PAST EDITIONS ------
			// ------------------------
			$this->past_editions = new WP_Plugin_Ecofim_Service_Past_Editions();
			add_shortcode('list_past_edition' , array(&$this->past_editions , 'list_past_edition'));
			// ------------------------
		}

		public function setting(){


		}

		public function regiter_script()
		{
			// -----------------------------------------
  			//wp_register_script('hub_ct_core', plugins_url('js/hub.js', __FILE__),array("jquery"));
			//wp_register_script('signalR', $this->host_service.'/Scripts/jquery.signalR-2.0.1.js' ,array("jquery"));
			//wp_register_script('hub', $this->host_service.'/signalr/hubs' ,array("jquery"));
			// -----------------------------------------
			//wp_register_script('underscore', 'http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js',array("jquery"));
			//wp_register_script('backbone', 'http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min.js',array("jquery"));
			//wp_register_script('app_eco_serv', plugins_url('js/ecofilm-services.js', __FILE__),array("jquery"));
			// -----------------------------------------
			$this->boletin->regiter_script();
			$this->comunicado->regiter_script();
			//$this->video_galery->regiter_script();
			$this->list_corto->regiter_script();
			//$this->programa->regiter_script();
			// -----------------------------------------
		}

		public function register_styles()
		{
			//wp_register_style('ccs_eco_serv', plugins_url('css/ecofilm-services.css', __FILE__));
			// -----------------------------------------
			$this->boletin->register_styles();
			$this->comunicado->register_styles();
			//$this->video_galery->register_styles();
			$this->list_corto->register_styles();
			//$this->programa->register_styles();
			// -----------------------------------------
		}

		public static function activate()
		{

		}

		public static function deactivate()
		{

		}
	}

}

if(class_exists('WP_Plugin_Ecofim_Service'))
{
	// ----------------------------------
	register_activation_hook(__FILE__, array('WP_Plugin_Ecofim_Service', 'activate'));
	register_deactivation_hook(__FILE__, array('WP_Plugin_Ecofim_Service', 'deactivate'));
	// ----------------------------------
	$wp_plugin_ecofilm_service = new WP_Plugin_Ecofim_Service();
	// ----------------------------------
}
?>
