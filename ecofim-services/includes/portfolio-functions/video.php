<?php 
// Access WordPress 
$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];
require_once( $path_to_wp . '/wp-load.php' );
require_once( $path_to_wp.'/wp-content/plugins/ecofim-services/ecofim-services-bussines.php' );


$bussines = new WP_Plugin_Ecofim_Service_Bussines();
$postid = stripslashes(htmlspecialchars_decode(filter_input(INPUT_GET, 'post-id', FILTER_SANITIZE_STRING)));
$corto = $bussines->get_corto_short($postid);

if(empty($video_height)) $video_height = 480;
 
wp_head(); 

wp_dequeue_script( 'my_acsearch' );  

?>
	<script>
	jQuery(document).ready(function($){
		if( $(window).width() <= 690 ){

			function pp_video_height() {
				$('#pp-video-wrap').css('height',$('.mejs-container').width()/1.777);
			}
			
			$(window).resize(pp_video_height);
			pp_video_height();
		}
	});	
	</script>
<style>
	#header-outer { display: none!Important;}
</style>
</head>

<div id="header-outer" data-header-resize="1"></div>

<style>
		body {background-color: #000; overflow-y:hidden; height: <?php echo ($video_height + 52); ?>px!Important;}
		#wpadminbar { display: none;}
		html { margin-top: 0px!important; }
		.mejs-mediaelement #me_flash_0_container {
			height: 100%;
		}
		.mejs-fullscreen-button {
			display: none!important;
		}
		@media only screen 
		and (min-width : 1px) and (max-width : 1050px) {
			body {background-color: transparent!important;}
		}
		.title h1{color:#fff!important;}
		.title h1 small{color:#27CCC0!important;}
		.view {
			border: thin solid #27CCC0;
		}
	</style>

<body class="pp-video-function">
	<div class="title">
		<h1><?=$corto['titulo_org']?> | <small><?=$corto['director']?></small></h1>
	</div>
<?php
switch ($corto['display']) {
	case 'vimeo':
		 $video ='<iframe class="view" src="http://player.vimeo.com/video/'.$corto['file'].'" width="720" height="405" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>'; 
		break;
	case 'youtube':
		$video = '<iframe class="view" src="//www.youtube.com/embed/'.$corto['file'].'" width="720" height="405" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>';
		break;
	case 'file':
		//  //
		break;
}

echo '<div id="pp-video-wrap">'.do_shortcode($video).'</div>';
wp_footer(); ?>