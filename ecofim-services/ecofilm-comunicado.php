<?
require 'ecofilm-csv.php';

if(!class_exists('WP_Plugin_Ecofim_Service_Comunicado'))
{
	class WP_Plugin_Ecofim_Service_Comunicado extends WP_Plugin_Ecofim_Service_csv
	{
		//-----------------------------------------
		//-----------------------------------------
		//-----------------------------------------
		public function regiter_script()
		{
			wp_register_script('app_eco_serv_comunicado', plugins_url('js/ecofilm-comunicado.js', __FILE__),array("jquery"));
		}
		
		public function register_styles()
		{
			wp_register_style('css_eco_serv_comunicado', plugins_url('css/ecofilm-comunicado.css', __FILE__));	
		}
		//-----------------------------------------
		//-----------------------------------------
		//-----------------------------------------
		public function listado_comunicado($atts, $content = null)
		{
			extract(
				shortcode_atts(array(
					//'lista_url' => 'https://docs.google.com/spreadsheet/pub?key=0Au0m6p_4_7q6dFpsSnRzYWlUTzZ6RWtvRi1WSG1nV1E&output=csv',
					'lista_url' => 'https://docs.google.com/spreadsheets/d/1HqdNRScSsEtSFgTe4yXqAB5zajkRX6TezK3jbzKRsjg/pub?output=csv',
					'count' => 'all' ,
					'comentario' => true ,
					'class' => 'listado_comunicado' ,
					), $atts));  
			//-----------------------------------------
			//-----------------------------------------
			wp_enqueue_style ( 'css_eco_serv_comunicado' );
			wp_enqueue_script( 'app_eco_serv_comunicado' );
			//-----------------------------------------
			//-----------------------------------------
			ob_start();
			
			$data = array_reverse($this->feedToArray($lista_url)); 		
			$break_count = ((int)$count > 0) ;
			$i = 0;
			?><ul class="<?=$class?>"><?
			foreach ($data as $comunicado) {
				if ( $break_count  && $i >= $count){
					break;
				}
				
				//$date = DateTime::createFromFormat( "d/m/Y", "01/07/1984");
				setlocale(LC_ALL,"es_ES.UTF-8");

				$href = $comunicado['Host'].$comunicado['Directory'].$comunicado['File'];
				?>
				<li class="item col boxed no-extra-padding">
					<div class="row">
						<div class="col span_12 col_last">
							<h3>							
								<?=$comunicado['Titulo']?>							
							</h3>
						</div>
						<div class="col span_12 col_last">							
							<a href="<?=$href?>" target="_blank">														
								<span style="color:#333333!important; line-height: 1.3em;">
									<?=$comunicado['Cometario']?>											
								</span>
								<? 
								$fecha = new StdClass;
								$fecha->innertext = $comunicado['Fecha'];//'25/08/2012';
								$eventDate = DateTime::createFromFormat('d/m/Y', $fecha->innertext);
								if ( false===$eventDate ) {
  									die('invalid date format');
								}
								?><br />
								<span style="font-size:.8em;"><?= strftime("%A, %d de %B de %Y", $eventDate->getTimestamp()) ?></span>
							</a>
						</div>
					</div>
				</li>				
				<?							
				$i++;
			}
			?></ul><?
			/*
			echo '<pre>';					
			print_r($data);		
			echo '</pre>';		
			*/	
			$list_markup = ob_get_contents();
			ob_end_clean();
			return $list_markup;		
		}
		
		public function ultimos_comunicado($atts, $content = null)
		{
			extract(
				shortcode_atts(array(					
					'count' => 1 ,
					'comentario' => false ,
					), $atts));  

			return do_shortcode('[listado_comunicado count="'.$count.'" comentario="'.$comentario.'"]'.$content.'[/listado_comunicado]');	
		}
		//-----------------------------------------
		//-----------------------------------------
		//-----------------------------------------
	}
	
}

?>